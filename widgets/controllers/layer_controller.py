import PyQt5.QtWidgets as QW
from PyQt5.QtCore import Qt, pyqtSignal, pyqtSlot
from PyQt5.QtSvg import QSvgWidget

from pathlib import Path
from collections import OrderedDict

from widgets.controllers.base_controller import BaseController
from widgets.layer_name_text_box import LayerNameTextBox
from widgets.qmlComponents.LayerWidget import Ui_LayerWidget
from widgets.icon_button import IconButton
from widgets.layer_list import LayerList
from widgets.combo_box import ComboBox

from definitions import GRAPHICS_DIR, BLEND_MODES, program_data, PLUS_ICON_PATH, MINUS_ICON_PATH, KEYFRAME_ICON_PATH

from utils.get_path_as_string import get_path_as_string
from utils.odict_insert_at_index import odict_insert_at_index


class LayerController(BaseController):
    layer_add = pyqtSignal(str, int)
    layer_remove = pyqtSignal(str)
    layer_change = pyqtSignal(str)
    layer_rename = pyqtSignal(str, str)
    layer_blend_mode_change = pyqtSignal(str, str)
    layer_opacity_change = pyqtSignal(str, int)
    layer_input_path_change = pyqtSignal(str, str, str)

    def __init__(self, *args, **kwargs):
        self.layers = OrderedDict()
        self.markers = {}
        self.layers['root'] = None
        self.emit_layer_change = True
        self.emit_clone_layer_combo_box_change = True

        super(LayerController, self).__init__(*args, **kwargs)

    def init_ui(self):
        Ui_LayerWidget().setupUi(self)
        self.children = {
            'cloneLayerComboBoxContainer': self.findChild(QW.QHBoxLayout, 'cloneLayerComboBoxContainer'),
            'changeLayerModeBtn':          self.findChild(QW.QPushButton, 'changeLayerModeBtn'),
            'inputModeLabel':              self.findChild(QW.QLabel, 'inputModeLabel'),
            'layersListContainer':         self.findChild(QW.QVBoxLayout, 'layersListContainer'),
            'layerBtnContainer':           self.findChild(QW.QFrame, 'layerBtnContainer'),
            'inputCloneLayerBtn':          self.findChild(QW.QPushButton, 'inputCloneLayerBtn'),
            'inputImageBtn':               self.findChild(QW.QPushButton, 'inputImageBtn'),
            'inputVideoBtn':               self.findChild(QW.QPushButton, 'inputVideoBtn'),
        }
        self.children['layersList'] = LayerList(self)
        self.children['layersListContainer'].addWidget(self.children['layersList'])

        self.children['cloneLayerComboBox'] = ComboBox(hide_selected=True, width=127, height=31, pos_type='top')
        self.children['cloneLayerComboBoxContainer'].addWidget(self.children['cloneLayerComboBox'])

        self.children['cloneLayerComboBox'].setHidden(True)
        self.children['changeLayerModeBtn'].setHidden(True)
        self.children['inputModeLabel'].setHidden(True)

        self.children['addLayerBtn'] = IconButton(PLUS_ICON_PATH, 40, 30)
        self.children['layerBtnContainer'].layout().addWidget(self.children['addLayerBtn'])

        self.children['delLayerBtn'] = IconButton(MINUS_ICON_PATH, 40, 30)
        self.children['delLayerBtn'].setEnabled(False)
        self.children['layerBtnContainer'].layout().addWidget(self.children['delLayerBtn'])

        self.children['layerBlendModeBox'] = ComboBox(hide_selected=True, width=127, height=30, pos_type='top')
        for name in BLEND_MODES.keys():
            self.children['layerBlendModeBox'].addItem(name)
        self.children['layerBtnContainer'].layout().addWidget(self.children['layerBlendModeBox'])

        self.children['layerOpacityBox'] = QW.QSpinBox()
        self.children['layerOpacityBox'].setMaximum(100)
        self.children['layerOpacityBox'].setMaximumWidth(60)
        self.children['layerOpacityBox'].setMinimumHeight(30)
        self.children['layerOpacityBox'].setFocusPolicy(Qt.ClickFocus)
        self.children['layerBtnContainer'].layout().addWidget(self.children['layerOpacityBox'])

        self.clone_layers_by_index = {}

        self.update_layers()

    def connect_signals(self):
        self.children['addLayerBtn'].clicked.connect(          self.add_layer_button_clicked)
        self.children['delLayerBtn'].clicked.connect(          self.del_layer_button_clicked)
        self.children['layersList'].currentItemChanged.connect(self.layers_list_item_changed)
        self.children['layersList'].itemDoubleClicked.connect( self.layer_item_double_clicked)

        self.children['inputCloneLayerBtn'].clicked.connect(self.input_clone_layer_button_clicked)
        self.children['inputImageBtn'].clicked.connect(     self.input_image_button_clicked)
        self.children['inputVideoBtn'].clicked.connect(     self.input_video_button_clicked)

        self.children['cloneLayerComboBox'].currentTextChanged.connect(self.clone_layer_combo_box_changed)
        self.children['changeLayerModeBtn'].clicked.connect(           self.change_input_mode_button_clicked)
        self.children['layerBlendModeBox'].currentTextChanged.connect( self.blend_mode_change)
        self.children['layerOpacityBox'].valueChanged.connect(         self.opacity_change)

        self.layer_add.connect(              program_data['manager'].add_layer)
        self.layer_remove.connect(           program_data['manager'].delete_layer)
        self.layer_change.connect(           program_data['manager'].select_layer)
        self.layer_rename.connect(           program_data['manager'].rename_layer)
        self.layer_opacity_change.connect(   program_data['manager'].change_layer_opacity)
        self.layer_blend_mode_change.connect(program_data['manager'].change_layer_blend_mode)
        self.layer_input_path_change.connect(program_data['manager'].change_layer_input_path)

        program_data['signals'].layer_added.connect(              self.add_layer)
        program_data['signals'].layer_deleted.connect(            self.delete_layer)
        program_data['signals'].layer_selected.connect(           self.select_layer)
        program_data['signals'].layer_renamed.connect(            self.rename_layer)
        program_data['signals'].layer_input_path_changed.connect( self.update_input_path)
        program_data['signals'].layer_blend_mode_changed.connect( self.set_blend_mode)
        program_data['signals'].layer_opacity_changed.connect(    self.set_opacity)
        program_data['signals'].layer_queue_order_changed.connect(self.update_layers)

        program_data['signals'].keyframe_selected.connect(self.select_frame)
        program_data['signals'].keyframe_added.connect(   self.select_frame)
        program_data['signals'].keyframe_changed.connect( self.select_frame)
        program_data['signals'].keyframe_deleted.connect( self.select_frame)

        # theres a bug related to resize events and listItemWidget visibility, this is the fix for now
        program_data['signals'].window_size_changed.connect(self.update_layers)

    def clone_layer_combo_box_changed(self, layer_name, layer_id):
        if self.emit_clone_layer_combo_box_change and self.emit_layer_change:
            self.layer_input_path_change.emit(program_data['selected_layer'], 'Clone', layer_id)

    def change_input_mode_button_clicked(self):
        if self.emit_layer_change:
            self.layer_input_path_change.emit(program_data['selected_layer'], None, None)
            self.select_layer(program_data['selected_layer'])

    def input_clone_layer_button_clicked(self):
        if self.emit_layer_change:
            self.layer_input_path_change.emit(program_data['selected_layer'], 'Clone', 'root')
            self.select_layer(program_data['selected_layer'])
            if len(program_data['layers']) > 2:
                self.children['cloneLayerComboBox'].showPopup()

    def input_image_button_clicked(self):
        if self.emit_layer_change:
            input_path = QW.QFileDialog.getOpenFileName(self, filter='*.png')[0]
            if input_path:
                self.layer_input_path_change.emit(program_data['selected_layer'], 'Image', input_path)
                self.select_layer(program_data['selected_layer'])

    def input_video_button_clicked(self):
        if self.emit_layer_change:
            input_path = QW.QFileDialog.getOpenFileName(self, filter='*.mkv')[0]
            if input_path:
                self.layer_input_path_change.emit(program_data['selected_layer'], 'Video', input_path)
                # self.session.layers[self.session.selectedLayer]['inputPath'] = VideoInputHandler(input_path)
                self.select_layer(program_data['selected_layer'])

    def update_input_path(self, layer_id, input_mode, input_path):
        if layer_id == program_data['selected_layer']:
            self.select_layer(layer_id)

    def update_clone_layer_combo_box(self, *args):
        if program_data['selected_layer_full']['input_mode'] == 'Clone':
            self.emit_clone_layer_combo_box_change = False
            self.children['cloneLayerComboBox'].clear()
            for layer_id in filter(lambda id: id != program_data['selected_layer']
                                   and program_data['layers'][id]['input_path'] != program_data['selected_layer'],
                                   program_data['layers']):
                self.children['cloneLayerComboBox'].addItem(program_data['layers'][layer_id]['name'], userData=layer_id)
            self.children['cloneLayerComboBox'].setCurrentText(program_data['layers'][program_data['selected_layer_full']['input_path']]['name'])
            self.emit_clone_layer_combo_box_change = True

    def update_input_label(self):
        layer = program_data['selected_layer_full']
        if layer['input_path']:
            if layer['input_mode'] == 'Clone':
                self.children['inputModeLabel'].setText('Mode: Clone')
            else:
                self.children['inputModeLabel'].setText('Mode: {}\nPath: {}'.format(layer['input_mode'], layer['input_path']))

    def blend_mode_change(self, blend_mode):
        self.layer_blend_mode_change.emit(program_data['selected_layer'], blend_mode.lower())

    def opacity_change(self, opacity):
        self.layer_opacity_change.emit(program_data['selected_layer'], opacity)

    def get_layer_id_from_item(self, layer_item):
        for layer_id, item in self.layers.items():
            if item == layer_item:
                return layer_id

    def layer_item_double_clicked(self, layer_item):
        layer_name = layer_item.text().lower()
        layer_id = self.get_layer_id_from_item(layer_item)
        if layer_name != list(self.layers)[0]:
            tmpNameBox = LayerNameTextBox(layer_id,
                                          layer_name,
                                          complete=self.complete_rename_layer,
                                          cancel=self.cancel_rename_layer)
            self.children['layersList'].setItemWidget(self.layers[layer_id], tmpNameBox)
            self.disable()

    def cancel_rename_layer(self, layer_id):
        self.children['layersList'].removeItemWidget(self.layers[layer_id])
        self.enable()
        self.update_layers()

    def complete_rename_layer(self, layer_name, layer_id):
        self.layer_rename.emit(layer_id, layer_name)
        self.enable()
        self.update_layers()

    def del_layer_button_clicked(self):
        layer_id = program_data['selected_layer']
        self.layer_remove.emit(layer_id)

    def disable(self):
        self.children['layerBlendModeBox'].setEnabled(False)
        self.children['layerOpacityBox'].setEnabled(False)
        self.children['delLayerBtn'].setEnabled(False)
        self.children['addLayerBtn'].setEnabled(False)

    def enable(self):
        self.children['layerBlendModeBox'].setEnabled(True)
        self.children['layerOpacityBox'].setEnabled(True)
        self.children['delLayerBtn'].setEnabled(True)
        self.children['addLayerBtn'].setEnabled(True)

    def layers_list_item_changed(self, layer_item):
        if self.emit_layer_change:
            if layer_item:
                layer_id = self.get_layer_id_from_item(layer_item)
                if layer_id in self.layers.keys():
                    self.layer_change.emit(layer_id)

    def add_layer_button_clicked(self):
        self.active_name_textbox = QW.QListWidgetItem()

        layer_name = 'Layer {}'.format(len(program_data['layers']))
        queue_order = (len(self.layers)
                       - self.children['layersList'].row(self.layers[program_data['selected_layer']])
                       + 1)
        self.layer_add.emit(layer_name, queue_order)

    def make_layer_list_item(self, title):
        item = QW.QListWidgetItem(title)
        marker = QW.QWidget()
        marker.setLayout(QW.QHBoxLayout())
        markerSvg = QSvgWidget(KEYFRAME_ICON_PATH)
        markerSvg.setFixedSize(13, 15)
        marker.layout().addStretch()
        marker.layout().addWidget(markerSvg)
        marker.layout().setContentsMargins(0, 0, 0, 0)
        markerSvg.setStyleSheet('background-color: transparent')
        marker.setStyleSheet('background-color: transparent')
        marker.setHidden(True)
        return item, marker

    def update_layers(self):
        self.children['layersList'].clear()
        for layer_id in reversed(program_data['layers'].keys()):
            list_item, list_marker = self.make_layer_list_item(program_data['layers'][layer_id]['name'])
            self.layers[layer_id]  = list_item
            self.markers[layer_id] = list_marker
            self.children['layersList'].addItem(list_item)
            self.children['layersList'].setItemWidget(list_item, list_marker)

        if program_data['selected_layer']:
            self.emit_layer_change = False
            self.children['layersList'].setCurrentRow(self.children['layersList'].row(self.layers[program_data['selected_layer']]))
            self.emit_layer_change = True

        self.select_frame(program_data['selected_frame'])

    @pyqtSlot(str, str)
    def set_blend_mode(self, __, blend_mode):
        self.children['layerBlendModeBox'].setCurrentText(blend_mode)

    @pyqtSlot(str, int)
    def set_opacity(self, __, opacity):
        self.children['layerOpacityBox'].setValue(opacity)

    @pyqtSlot(str, str)
    def rename_layer(self, layer_id, layer_name):
        self.layers[layer_id].setText(layer_name)

    @pyqtSlot(str)
    def delete_layer(self, layer_id):
        self.update_layers()

    @pyqtSlot(str, int)
    def add_layer(self, layer_id, queue_order):
        self.update_layers()
        self.layer_change.emit(layer_id)

    @pyqtSlot()
    def select_frame(self, *args, **kwargs):
        frame = program_data['selected_frame']
        layers_with_keyframes = []
        for layer_id in program_data['layers'].keys():
            if frame in program_data['keyframes'].keys():
                if layer_id in program_data['keyframes'][frame]:
                    layers_with_keyframes.append(layer_id)
                else:
                    for effect_id in program_data['manager'].get_effects_in_layer(layer_id):
                        if effect_id in program_data['keyframes'][frame].keys():
                            layers_with_keyframes.append(layer_id)
            self.markers[layer_id].setHidden(True)

        for layer_id in layers_with_keyframes:
            self.markers[layer_id].setHidden(False)

    @pyqtSlot(str)
    def select_layer(self, layer_id):
        self.emit_layer_change = False
        self.children['layersList'].setCurrentRow(self.children['layersList'].row(self.layers[layer_id]))
        self.select_frame()

        if layer_id == 'root':
            self.children['addLayerBtn'].setEnabled(True)
            self.children['layerBlendModeBox'].setEnabled(False)
            self.children['layerOpacityBox'].setEnabled(False)
            self.children['delLayerBtn'].setEnabled(False)
            self.children['inputCloneLayerBtn'].setEnabled(False)
        else:
            self.children['layerBlendModeBox'].setEnabled(True)
            self.children['layerOpacityBox'].setEnabled(True)
            self.children['delLayerBtn'].setEnabled(True)
            self.children['inputCloneLayerBtn'].setEnabled(True)

        self.set_blend_mode(None, program_data['selected_layer_full']['blend_mode'])
        self.children['layerOpacityBox'].findChild(QW.QLineEdit).deselect()
        self.set_opacity(None, program_data['selected_layer_full']['opacity'])

        if program_data['layers'][layer_id]['input_path']:
            self.children['inputCloneLayerBtn'].setHidden(True)
            self.children['inputImageBtn'].setHidden(True)
            self.children['inputVideoBtn'].setHidden(True)
            self.update_input_label()
            self.children['inputModeLabel'].setHidden(False)
            self.children['changeLayerModeBtn'].setHidden(False)
            if program_data['selected_layer_full']['input_mode'] == 'Clone':
                self.update_clone_layer_combo_box()
                self.children['cloneLayerComboBox'].setHidden(False)
            else:
                self.children['cloneLayerComboBox'].setHidden(True)
        else:
            self.children['inputCloneLayerBtn'].setHidden(False)
            self.children['inputImageBtn'].setHidden(False)
            self.children['inputVideoBtn'].setHidden(False)
            self.children['inputModeLabel'].setHidden(True)
            self.children['cloneLayerComboBox'].setHidden(True)
            self.children['changeLayerModeBtn'].setHidden(True)

        self.emit_layer_change = True

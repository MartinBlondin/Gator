from collections import OrderedDict
from pathlib import Path

import PyQt5.QtWidgets as QW
from PyQt5.QtCore import pyqtSignal, pyqtSlot

from utils.odict_insert_at_index import odict_insert_at_index
from utils.get_path_as_string import get_path_as_string

from widgets.controllers.base_controller import BaseController
from widgets.controllers.effect_controller import EffectController
from widgets.controllers.generator_controller import GeneratorController

from widgets.qmlComponents.ModulesGridWidget import Ui_ModulesGridWidget

from widgets.animator_keyframe_settings_dialog import AnimatorKeyframeSettingsDialog
from widgets.icon_button import IconButton
from widgets.combo_box import ComboBox

from definitions import program_data, GRAPHICS_DIR, PLUS_ICON_PATH


class ModulesController(BaseController):
    def __init__(self, *args, **kwargs):
        self.active_module = 'Effects'
        self.modules = {'Effects':    {'controller': EffectController,
                                       'widget': None},
                        'Generators': {'controller': GeneratorController,
                                       'widget': None}}
        super(ModulesController, self).__init__(*args, **kwargs)

    def init_ui(self):
        Ui_ModulesGridWidget().setupUi(self)
        self.children = {
            'modulesArea':          self.findChild(QW.QWidget, 'modulesArea'),
            'modulesScrollArea':    self.findChild(QW.QScrollArea, 'modulesScrollArea'),
            'addButtonContainer':   self.findChild(QW.QFrame, 'addButtonContainer'),
            'addModulesBtn':        IconButton(PLUS_ICON_PATH, 40, 40),
        }
        self.children['modulesContainer']     = QW.QHBoxLayout(self.children['modulesArea'])
        self.children['activeModuleComboBox'] = ComboBox(self.children['modulesArea'], hide_selected=True, width=116)

        self.children['addButtonContainer'].layout().addWidget(self.children['addModulesBtn'])

        for module_name, module in self.modules.items():
            self.children['activeModuleComboBox'].addItem(module_name)
            module['widget'] = module['controller'](self)
            self.children['modulesContainer'].addWidget(module['widget'])
            if module_name != self.active_module:
                module['widget'].setHidden(True)

        self.animator_keyframe_settings_dialog = AnimatorKeyframeSettingsDialog()

    def connect_signals(self):
        self.children['addModulesBtn'].clicked.connect(self.add_modules_button_clicked)
        self.children['activeModuleComboBox'].currentTextChanged.connect(self.on_active_module_changed)
        program_data['signals'].window_size_changed.connect(self.on_resize)

    def add_modules_button_clicked(self):
        self.modules[self.active_module]['widget'].add_button_clicked()

    def show_animator_keyframe_settings_dialog(self, pos, animator_id, attribute_name):
        self.animator_keyframe_settings_dialog.setup(pos, animator_id, attribute_name)

    @pyqtSlot()
    def refresh_ui(self, *args, **kwargs):
        x_offset = self.modules[self.active_module]['widget'].width
        self.children['addButtonContainer'].move(x_offset, 0)

        area_width = self.width()
        if x_offset + 100 > area_width: area_width = x_offset + 100
        self.children['modulesArea'].setFixedWidth(area_width)
        self.children['modulesArea'].setFixedHeight(self.height())

        combo_x = self.children['modulesArea'].rect().width() - self.children['activeModuleComboBox'].rect().width()
        combo_y = self.children['modulesArea'].rect().height() - 35
        self.children['activeModuleComboBox'].move(combo_x, combo_y)

    @pyqtSlot(int, int)
    def on_resize(self, *args):
        self.children['addButtonContainer'].setFixedHeight(self.height())
        self.refresh_ui()

    def on_active_module_changed(self, module):
        self.modules[self.active_module]['widget'].setHidden(True)
        self.active_module = module
        self.modules[self.active_module]['widget'].setHidden(False)
        self.modules[self.active_module]['widget'].refresh_ui()
        self.refresh_ui()

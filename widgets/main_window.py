from contextlib import suppress
from pathlib import Path

import PyQt5.QtWidgets as QW

from widgets.qmlComponents.MainLayout import Ui_MainLayout

from widgets.controllers.generator_controller import GeneratorController
from widgets.controllers.timeline_controller  import TimelineController
from widgets.controllers.toolbar_controller   import ToolbarController
from widgets.controllers.preview_controller   import PreviewController
from widgets.controllers.modules_controller   import ModulesController
from widgets.controllers.effect_controller    import EffectController
from widgets.controllers.layer_controller     import LayerController

from backend.managers.data_manager import DataManager

from utils.get_path_as_string import get_path_as_string
from utils.debug_buttons import DebugButtons
from utils.dict_watcher import DictWatcher

from definitions import QSS_DIR, ROOT_DIR, program_data, DEBUG, SHORTCUTS


class MainWindow(QW.QMainWindow):
    def __init__(self, desktop):
        super(MainWindow, self).__init__()
        self.setWindowTitle('QTTestApp')

        self.desktop = desktop
        self.data_manager = DataManager()
        program_data['manager'].set_main_window(self)

        self.init_ui()

        self.data_manager.select_layer('root')
        self.data_manager.select_keyframe('1')
        program_data['manager'].set_window_size(self.width(), self.height())

        if DEBUG:
            self.dict_watcher = DictWatcher(program_data, ['generators', 'keyframes'], style=program_data['manager'].get_stylesheet()); program_data['signals'].shutdown.connect(self.dict_watcher.close)
            self.debug_buttons = DebugButtons(program_data['manager'].get_stylesheet()); program_data['signals'].shutdown.connect(self.debug_buttons.close)
            # self.debug_buttons.add_button('stats', lambda: program_data['manager'].stats())
            program_data['manager'].change_layer_input_path('root', 'Image', get_path_as_string(Path(ROOT_DIR, 'tmp_input_image.jpg')))
            # program_data['manager'].add_effect('gmic.Cartoon', 'root', None)
            program_data['manager'].add_effect('gmic.Kaleidoscope [blended]', 'root', 1)
            # program_data['manager'].add_keyframe_target('1', 'Iterations')
            program_data['manager'].add_generator()
            # program_data['manager'].add_layer('Hey')
            # program_data['manager'].add_layer('He')
            # program_data['manager'].add_layer('H')
            # program_data['manager'].change_layer_input_path('1', 'Clone', 'root')
            # program_data['manager'].add_effect('gmic.Cartoon', '2', None)
            # program_data['manager'].add_keyframe('7', '1', 'Smoothness', 4)
            # program_data['manager'].add_keyframe('12', '1', 'Smoothness', 3.1)
            # program_data['manager'].delete_effect('2')

    def init_ui(self):
        self.setStyleSheet(program_data['manager'].get_stylesheet())

        self.setCentralWidget(QW.QWidget())
        Ui_MainLayout().setupUi(self.centralWidget())
        self.centralWidget().show()

        self.add_to_container(LayerController,    'layerContainer')
        self.add_to_container(TimelineController, 'timelineContainer')
        self.add_to_container(ToolbarController,  'toolbarContainer')
        self.add_to_container(PreviewController,  'previewContainer')
        self.add_to_container(ModulesController,  'modulesContainer')

        window_size = self.desktop.screenGeometry()
        width = window_size.width()
        height = window_size.height()
        self.setGeometry(width / 0.1, height / 0.1,
                         width / 1.1, height / 1.1)
        self.setup_menu_bar()
        self.show()

    def setup_menu_bar(self):
        bar = self.menuBar()
        bar_actions = {
            'File': {
                'New':     [SHORTCUTS['new'],     self.new],
                'Save':    [SHORTCUTS['save'],    self.save],
                'Save As': [SHORTCUTS['save as'], self.saveAs],
                'Load':    [SHORTCUTS['load'],    self.load],
                'Exit':    [SHORTCUTS['exit'],    self.exit]},

            'Edit': {
                'Undo': [SHORTCUTS['undo'], program_data['manager'].undo],
                'Redo': [SHORTCUTS['redo'], program_data['manager'].redo]},
        }

        for menu, actions in bar_actions.items():
            menu = bar.addMenu(menu)
            for title, data in actions.items():
                action = QW.QAction(title, self)
                if data[0]: action.setShortcut(data[0])
                if data[1]: action.triggered.connect(data[1])
                menu.addAction(action)

    def exit(self):
        if program_data['last_save_on_change'] != program_data['current_change']:
            # warn about unsaved progress
            # choice = warn('You have unsaved progress', ['Cancel', 'Save and exit', 'Exit'])
            # if choice == 'Exit'
            pass
        program_data['manager'].shutdown()

    def new(self):
        program_data['manager'].clear()

    def load(self):
        name = QW.QFileDialog.getOpenFileName(self, filter='*.gator')[0]
        if name: program_data['manager'].load(name)

    def save(self):
        if not program_data['last_save_file_name']: self.saveAs()
        else:                                       program_data['manager'].save()

    def saveAs(self):
        name = QW.QFileDialog.getSaveFileName(self, filter='*.gator')[0]
        if name: program_data['manager'].save(name)

    def resizeEvent(self, event):
        super(MainWindow, self).resizeEvent(event)
        program_data['manager'].set_window_size(self.width(), self.height())

    def add_to_container(self, child, container, widget=QW.QFrame):
        container = self.findChild(widget, container)
        container.layout().addWidget(child(parent=container))

    def closeEvent(self, event):
        super(MainWindow, self).closeEvent(event)
        program_data['manager'].shutdown()

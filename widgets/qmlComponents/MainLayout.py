# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/martin/python/qt/Gator/static/qml/MainLayout.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainLayout(object):
    def setupUi(self, MainLayout):
        MainLayout.setObjectName("MainLayout")
        MainLayout.resize(1276, 809)
        MainLayout.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.verticalLayout_10 = QtWidgets.QVBoxLayout(MainLayout)
        self.verticalLayout_10.setContentsMargins(-1, 6, -1, 6)
        self.verticalLayout_10.setSpacing(0)
        self.verticalLayout_10.setObjectName("verticalLayout_10")
        self.verticalLayout_7 = QtWidgets.QVBoxLayout()
        self.verticalLayout_7.setContentsMargins(-1, -1, 1, -1)
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setSpacing(4)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.previewContainer = QtWidgets.QFrame(MainLayout)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.previewContainer.sizePolicy().hasHeightForWidth())
        self.previewContainer.setSizePolicy(sizePolicy)
        self.previewContainer.setMinimumSize(QtCore.QSize(0, 200))
        self.previewContainer.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.previewContainer.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.previewContainer.setFrameShadow(QtWidgets.QFrame.Raised)
        self.previewContainer.setObjectName("previewContainer")
        self.horizontalLayout_10 = QtWidgets.QHBoxLayout(self.previewContainer)
        self.horizontalLayout_10.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_10.setSpacing(0)
        self.horizontalLayout_10.setObjectName("horizontalLayout_10")
        self.verticalLayout_3.addWidget(self.previewContainer)
        self.modulesContainer = QtWidgets.QFrame(MainLayout)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.modulesContainer.sizePolicy().hasHeightForWidth())
        self.modulesContainer.setSizePolicy(sizePolicy)
        self.modulesContainer.setMinimumSize(QtCore.QSize(0, 315))
        self.modulesContainer.setObjectName("modulesContainer")
        self.effectCont = QtWidgets.QVBoxLayout(self.modulesContainer)
        self.effectCont.setContentsMargins(0, 0, 0, 0)
        self.effectCont.setObjectName("effectCont")
        self.verticalLayout_3.addWidget(self.modulesContainer)
        self.timelineScrollerContainer = QtWidgets.QHBoxLayout()
        self.timelineScrollerContainer.setObjectName("timelineScrollerContainer")
        self.verticalLayout_3.addLayout(self.timelineScrollerContainer)
        self.timelineContainer = QtWidgets.QFrame(MainLayout)
        self.timelineContainer.setMinimumSize(QtCore.QSize(200, 100))
        self.timelineContainer.setMaximumSize(QtCore.QSize(16777215, 100))
        self.timelineContainer.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.timelineContainer.setFrameShadow(QtWidgets.QFrame.Raised)
        self.timelineContainer.setObjectName("timelineContainer")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.timelineContainer)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout_3.addWidget(self.timelineContainer)
        self.verticalLayout_3.setStretch(1, 1)
        self.horizontalLayout_2.addLayout(self.verticalLayout_3)
        self.layerContainer = QtWidgets.QFrame(MainLayout)
        self.layerContainer.setStyleSheet("")
        self.layerContainer.setObjectName("layerContainer")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.layerContainer)
        self.verticalLayout.setContentsMargins(3, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_2.addWidget(self.layerContainer)
        self.horizontalLayout_2.setStretch(0, 16)
        self.horizontalLayout_2.setStretch(1, 1)
        self.verticalLayout_7.addLayout(self.horizontalLayout_2)
        self.verticalLayout_10.addLayout(self.verticalLayout_7)
        self.toolbarContainer = QtWidgets.QFrame(MainLayout)
        self.toolbarContainer.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.toolbarContainer.setFrameShadow(QtWidgets.QFrame.Raised)
        self.toolbarContainer.setObjectName("toolbarContainer")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.toolbarContainer)
        self.horizontalLayout.setContentsMargins(0, 3, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout_10.addWidget(self.toolbarContainer)
        self.verticalLayout_10.setStretch(0, 1)

        self.retranslateUi(MainLayout)
        QtCore.QMetaObject.connectSlotsByName(MainLayout)

    def retranslateUi(self, MainLayout):
        _translate = QtCore.QCoreApplication.translate
        MainLayout.setWindowTitle(_translate("MainLayout", "Form"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainLayout = QtWidgets.QWidget()
    ui = Ui_MainLayout()
    ui.setupUi(MainLayout)
    MainLayout.show()
    sys.exit(app.exec_())

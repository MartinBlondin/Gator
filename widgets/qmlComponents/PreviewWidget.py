# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/martin/python/qt/Gator/static/qml/PreviewWidget.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_PreviewWidget(object):
    def setupUi(self, PreviewWidget):
        PreviewWidget.setObjectName("PreviewWidget")
        PreviewWidget.resize(717, 380)
        self.verticalLayout = QtWidgets.QVBoxLayout(PreviewWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.frame = QtWidgets.QFrame(PreviewWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame.sizePolicy().hasHeightForWidth())
        self.frame.setSizePolicy(sizePolicy)
        self.frame.setMinimumSize(QtCore.QSize(0, 200))
        self.frame.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.frame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.horizontalLayout_10 = QtWidgets.QHBoxLayout(self.frame)
        self.horizontalLayout_10.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_10.setSpacing(0)
        self.horizontalLayout_10.setObjectName("horizontalLayout_10")
        self.layerPreviewContainer = QtWidgets.QFrame(self.frame)
        self.layerPreviewContainer.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.layerPreviewContainer.setFrameShadow(QtWidgets.QFrame.Raised)
        self.layerPreviewContainer.setObjectName("layerPreviewContainer")
        self.verticalLayout_9 = QtWidgets.QVBoxLayout(self.layerPreviewContainer)
        self.verticalLayout_9.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_9.setSpacing(0)
        self.verticalLayout_9.setObjectName("verticalLayout_9")
        self.horizontalLayout_10.addWidget(self.layerPreviewContainer)
        self.mainPreviewContainer = QtWidgets.QFrame(self.frame)
        self.mainPreviewContainer.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.mainPreviewContainer.setFrameShadow(QtWidgets.QFrame.Raised)
        self.mainPreviewContainer.setObjectName("mainPreviewContainer")
        self.verticalLayout_8 = QtWidgets.QVBoxLayout(self.mainPreviewContainer)
        self.verticalLayout_8.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_8.setSpacing(0)
        self.verticalLayout_8.setObjectName("verticalLayout_8")
        self.horizontalLayout_10.addWidget(self.mainPreviewContainer)
        self.verticalLayout.addWidget(self.frame)

        self.retranslateUi(PreviewWidget)
        QtCore.QMetaObject.connectSlotsByName(PreviewWidget)

    def retranslateUi(self, PreviewWidget):
        _translate = QtCore.QCoreApplication.translate
        PreviewWidget.setWindowTitle(_translate("PreviewWidget", "Form"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    PreviewWidget = QtWidgets.QWidget()
    ui = Ui_PreviewWidget()
    ui.setupUi(PreviewWidget)
    PreviewWidget.show()
    sys.exit(app.exec_())

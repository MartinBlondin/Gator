# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/martin/python/qt/Gator/static/qml/ModuleEditor.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_ModuleEditor(object):
    def setupUi(self, ModuleEditor):
        ModuleEditor.setObjectName("ModuleEditor")
        ModuleEditor.resize(277, 439)
        self.verticalLayout = QtWidgets.QVBoxLayout(ModuleEditor)
        self.verticalLayout.setContentsMargins(0, 0, 0, 12)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.frame_2 = QtWidgets.QFrame(ModuleEditor)
        self.frame_2.setMaximumSize(QtCore.QSize(270, 16777215))
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.frame_2)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.moduleToolbar = QtWidgets.QWidget(self.frame_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.moduleToolbar.sizePolicy().hasHeightForWidth())
        self.moduleToolbar.setSizePolicy(sizePolicy)
        self.moduleToolbar.setMinimumSize(QtCore.QSize(0, 40))
        self.moduleToolbar.setMaximumSize(QtCore.QSize(16777215, 40))
        self.moduleToolbar.setObjectName("moduleToolbar")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.moduleToolbar)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout_2.addWidget(self.moduleToolbar)
        self.scrollArea = QtWidgets.QScrollArea(self.frame_2)
        self.scrollArea.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.scrollArea.setStyleSheet("")
        self.scrollArea.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.scrollArea.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.scrollArea.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaContainer = QtWidgets.QWidget()
        self.scrollAreaContainer.setGeometry(QtCore.QRect(0, 0, 250, 361))
        self.scrollAreaContainer.setMinimumSize(QtCore.QSize(0, 0))
        self.scrollAreaContainer.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.scrollAreaContainer.setObjectName("scrollAreaContainer")
        self.attributeSettingsContainer = QtWidgets.QWidget(self.scrollAreaContainer)
        self.attributeSettingsContainer.setGeometry(QtCore.QRect(0, -10, 241, 411))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.attributeSettingsContainer.sizePolicy().hasHeightForWidth())
        self.attributeSettingsContainer.setSizePolicy(sizePolicy)
        self.attributeSettingsContainer.setMinimumSize(QtCore.QSize(0, 0))
        self.attributeSettingsContainer.setObjectName("attributeSettingsContainer")
        self.scrollArea.setWidget(self.scrollAreaContainer)
        self.verticalLayout_2.addWidget(self.scrollArea)
        self.verticalLayout_2.setStretch(1, 1)
        self.verticalLayout.addWidget(self.frame_2)

        self.retranslateUi(ModuleEditor)
        QtCore.QMetaObject.connectSlotsByName(ModuleEditor)

    def retranslateUi(self, ModuleEditor):
        _translate = QtCore.QCoreApplication.translate
        ModuleEditor.setWindowTitle(_translate("ModuleEditor", "Form"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ModuleEditor = QtWidgets.QWidget()
    ui = Ui_ModuleEditor()
    ui.setupUi(ModuleEditor)
    ModuleEditor.show()
    sys.exit(app.exec_())

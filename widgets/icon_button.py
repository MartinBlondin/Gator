import PyQt5.QtWidgets as QW
from PyQt5.QtSvg import QSvgWidget
from PyQt5.QtCore import Qt, QRect, pyqtSignal

from pathlib import Path

from widgets.base_graphics_view_button import BaseGraphicsViewButton

from utils.get_path_as_string import get_path_as_string

from definitions import GRAPHICS_DIR

BUTTON_GRADIENT_ICON   = get_path_as_string(Path(GRAPHICS_DIR, 'buttonGradient.svg'))
BUTTON_BACKGROUND_ICON = get_path_as_string(Path(GRAPHICS_DIR, 'buttonBackground.svg'))
BUTTON_HOVER_ICON      = get_path_as_string(Path(GRAPHICS_DIR, 'buttonHover.svg'))
BUTTON_DISABLED_ICON   = get_path_as_string(Path(GRAPHICS_DIR, 'buttonDisabled.svg'))


class IconButton(BaseGraphicsViewButton):
    clicked = pyqtSignal()
    toggled = pyqtSignal(bool)

    def __init__(self, icon_path, width, height,
                 data=None, toggle=False, toggled_icon_path=None, hide_background=False):
        super(IconButton, self).__init__(width=width, height=height, bradius=2)
        self.toggle = toggle
        self.toggled_icon = None
        self.toggle_state = False
        self.data = data

        self.hide_background = hide_background

        if self.hide_background:
            self.background.setHidden(True)
            self.outline.setHidden(True)

        self.icon = QSvgWidget(icon_path)
        self.icon.setStyleSheet('background-color: transparent;')
        scale = 2.5
        with open(icon_path, 'r+') as f:
            s = f.read()
            w = float(s.split('width="')[1].split('"')[0][:-2])
            h = float(s.split('height="')[1].split('"')[0][:-2])
        aspectRatio = h / w
        self.icon.setMaximumSize(self.width / scale, (self.height / scale) * aspectRatio)
        self.icon.setMinimumSize(self.width / scale, (self.height / scale) * aspectRatio)
        self.icon.move((self.width / 2) - (self.icon.width() / 2),
                       (self.height / 2) - (self.icon.height() / 2))
        self.graphics_scene.addWidget(self.icon)

        if toggled_icon_path:
            self.toggled_icon = QSvgWidget(toggled_icon_path)
            self.toggled_icon.setStyleSheet('background-color: transparent;')
            scale = 2.5
            with open(toggled_icon_path, 'r+') as f:
                s = f.read()
                w = float(s.split('width="')[1].split('"')[0][:-2])
                h = float(s.split('height="')[1].split('"')[0][:-2])
            aspectRatio = h / w
            self.toggled_icon.setMaximumSize(self.width / scale, (self.height / scale) * aspectRatio)
            self.toggled_icon.setMinimumSize(self.width / scale, (self.height / scale) * aspectRatio)
            self.toggled_icon.move((self.width / 2) - (self.toggled_icon.width() / 2),
                        (self.height / 2) - (self.toggled_icon.height() / 2))
            self.graphics_scene.addWidget(self.toggled_icon)
            self.toggled_icon.setHidden(True)

    def set_toggled(self, toggle_state):
        if self.toggle_state != toggle_state:
            self.on_toggle()

    def on_toggle(self):
        if self.toggle_state:
            self.toggle_state = False
            self.icon.setHidden(False)
            self.toggled_icon.setHidden(True)
        else:
            self.toggle_state = True
            self.icon.setHidden(True)
            self.toggled_icon.setHidden(False)
        self.toggled.emit(self.toggle_state)

    def mouseReleaseEvent(self, event):
        super(IconButton, self).mouseReleaseEvent(event, passthrough=True)
        if self.enabled and not self.ignore_mouse_events:
            self.pressing_mouse = False
            if self.toggle: self.on_toggle()
            else:
                self.updateMouseInsidePanel()
                if self.mouseInsidePanel: self.clicked.emit()
                self.set_hovered(self.mouseInsidePanel)

    def hide(self, hidden):
        if not self.hide_background:
            opacity = int(not hidden) * 100
            self.background.setOpacity(opacity)
            self.outline.setOpacity(opacity)
        self.icon.setHidden(hidden)
        if self.toggle:
            self.toggled_icon.setHidden(hidden)
        self.hidden_changed.emit(hidden)

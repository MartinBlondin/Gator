import PyQt5.QtWidgets as QW
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import Qt, pyqtSlot

from utils.get_path_as_string import get_path_as_string

from definitions import program_data, GRAPHICS_DIR

GRAPHICS_LOADING_IMAGE = get_path_as_string(GRAPHICS_DIR, 'loading.png')


class PreviewImage(QW.QLabel):
    def __init__(self, parent):
        super(PreviewImage, self).__init__()

        self.parent = parent

        self.setAlignment(Qt.AlignCenter)

        self.width = self.parent.width()
        self.height = self.parent.height()

        self.pixmap = QPixmap(GRAPHICS_LOADING_IMAGE)
        self.update_pixmap()

        program_data['signals'].window_size_changed.connect(self.on_resize)

    def update_pixmap(self):
        self.setPixmap(self.pixmap.scaledToWidth(self.width).scaledToHeight(self.height))

    def set_image(self, image):
        if image:
            self.pixmap = QPixmap.fromImage(image)
        else:
            self.pixmap = QPixmap(GRAPHICS_LOADING_IMAGE)
        self.update_pixmap()

    def clearImage(self):
        self.pixmap = QPixmap(GRAPHICS_LOADING_IMAGE)
        self.update_pixmap()

    @pyqtSlot()
    def on_resize(self):
        self.width = self.parent.width()
        self.height = self.parent.height()
        self.update_pixmap()

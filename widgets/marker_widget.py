from PyQt5.QtCore import QPoint, QRect
from PyQt5 import QtWidgets as QW
from PyQt5.QtSvg import QSvgWidget
from PyQt5.QtGui import QColor, QPainterPath, QPolygon

from utils.get_path_as_string import get_path_as_string

from definitions import GRAPHICS_DIR, KEYFRAME_ICON_PATH, KEYFRAME_SELECTED_ICON_PATH

'''
This should be a QPainterItem class but due to rendering issues(bad antialiasing and a white outline on my paths)
i have used an SVG instead
'''

class MarkerWidget(QSvgWidget):
    def __init__(self, x, y, width, height, *args, **kwargs):
        super(MarkerWidget, self).__init__(KEYFRAME_ICON_PATH, *args, **kwargs)
        self.move(x, y)
        self.width = width
        self.height = height
        self.setFixedSize(width, height)
        self.setStyleSheet('background-color: transparent;')
        self.selected = False

    def set_selected(self, selected):
        self.selected = selected
        if self.selected:
            self.load(KEYFRAME_SELECTED_ICON_PATH)
        else:
            self.load(KEYFRAME_ICON_PATH)

    def center(self):
        return QPoint(self.x() + self.width / 2, self.y() + self.height / 2)

    def get_rect(self, rect_type=QRect):
        return rect_type(self.x(), self.y(), self.width, self.height)

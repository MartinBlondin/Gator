from PyQt5.QtWidgets import QLineEdit, QShortcut
from PyQt5.QtGui import QKeySequence


class LayerNameTextBox(QLineEdit):
    def __init__(self, layer_id, default_name, complete, cancel, *args, **kwargs):
        super(LayerNameTextBox, self).__init__()

        self.layer_id = layer_id
        self.default_name = default_name
        self.insert(default_name)
        self.selectAll()
        self.complete = complete
        self.cancel = cancel
        self.completed = False

        QShortcut(QKeySequence("return"), self).activated.connect(self.confirm)

    def focusOutEvent(self, event):
        if not self.completed:
            self.cancel(self.layer_id)

        super(LayerNameTextBox, self).focusOutEvent(event)

    def keyPressEvent(self, event):
        super(LayerNameTextBox, self).keyPressEvent(event)

        if len(self.text()):
            if self.text()[0] == ' ':
                self.clear()
            else:
                self.setText(self.text()[0].upper() + self.text()[1:])

    def confirm(self):
        if self.text() != '' :
            self.completed = True
            self.complete(self.text(), self.layer_id)


import PyQt5.QtWidgets as QW
from PyQt5.QtCore import Qt

from widgets.marker_widget import MarkerWidget
from utils.number_within_range import number_within_range
from backend.managers.attribute_value_manager_mixin import get_next_attribute_value_at_frame

from definitions import program_data, COLORS


class TimelineAnimatorWidget(QW.QWidget):
    def __init__(self, parent, animator_id, name, x, y_offset, width, *args, **kwargs):
        super(TimelineAnimatorWidget, self).__init__(*args, **kwargs)
        self.parent = parent
        self.animator_id = animator_id
        self.name = name
        self.y_offset = y_offset
        self.x = x
        self.width = width
        self.setLayout(QW.QVBoxLayout())
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.setStyleSheet('background-color: transparent;font-family:Roboto Mono; font-weight: bold;color: {}'.format(COLORS['foreground_dark']))
        self.folded = False
        self.hidden = False
        self.visible_markers = []

        self.header = QW.QLabel(self.name)
        self.header.setStyleSheet('font-size: 16pt;')
        self.header.setFixedWidth(self.width)
        self.header.setFixedHeight(self.parent.header_height)
        self.header.setAlignment(Qt.AlignLeft)
        self.layout().addWidget(self.header)
        self.hidden_attributes = []

        self.attribute_markers = {}
        self.header_markers = {}

        self.attributes = {}
        self.update_position()
        self.update_attributes()

        self.connect_signals()

    def connect_signals(self):
        program_data['signals'].effect_name_changed.connect(self.on_effect_name_change)

    def set_y_offset(self, y_offset):
        self.y_offset = y_offset
        self.update_position()

    def mousePressEvent(self, event):
        super(TimelineAnimatorWidget, self).mousePressEvent(event)
        if self.hidden: return
        if event.button() == 1:
            if number_within_range(self.parent.mouse_position.y(), self.pos().y(), self.pos().y() + self.header.height()):
                self.fold()

    def get_markers(self):
        for frame, attributes in self.attribute_markers.items():
            for attribute_name, marker in attributes.items():
                yield(frame, attribute_name, marker)
        for frame, marker in self.header_markers.items():
            yield(frame, 'Header', marker)

    def fold(self):
        if self.hidden: return
        if self.folded:
            self.folded = False
            self.header.setStyleSheet('font-size: 14pt;')
            self.header.setFixedHeight(self.parent.header_height)
            self.header.setAlignment(Qt.AlignLeft)
        else:
            self.folded = True
            self.header.setStyleSheet('font-size: 16pt;')
            self.header.setFixedHeight(self.parent.attribute_height)
            self.header.setAlignment(Qt.AlignRight)

        for attribute_name, attribute in self.attributes.items():
            if attribute_name not in self.hidden_attributes:
                attribute.setHidden(self.folded)

        self.parent.update_ui()
        self.parent.selection.clear_selection()

    def update_position(self):
        if self.hidden: return
        self.move(self.x, self.y_offset)

        attribute_y_offset = self.y_offset
        if self.folded:
            for frame, marker in self.header_markers.items():
                x = (self.parent.get_x_from_frame(int(frame))
                     + (self.parent.frame_distance / 2)
                     - marker.rect().width() / 2)
                if x > self.parent.lines_start_position:
                    marker.move(x, attribute_y_offset)
                    if self.parent.selection.is_selected(frame, self.animator_id, 'Header') and self.parent.hide_selected: continue
                    marker.setHidden(False)
                else:
                    marker.setHidden(True)
            for frame, markers in self.attribute_markers.items():
                for attribute_name, marker in markers.items():
                    marker.setHidden(True)
        else:
            attribute_y_offset += self.parent.header_height
            for attribute_name, attribute in self.attributes.items():
                if attribute_name in self.hidden_attributes: continue
                for frame, markers in self.attribute_markers.items():
                    if attribute_name in markers:
                        x = (self.parent.get_x_from_frame(int(frame))
                             + (self.parent.frame_distance / 2)
                             - markers[attribute_name].rect().width() / 2)
                        if x > self.parent.lines_start_position:
                            markers[attribute_name].move(x, attribute.y() + self.y_offset)
                            if self.parent.selection.is_selected(frame, self.animator_id, attribute_name) and self.parent.hide_selected: continue
                            markers[attribute_name].setHidden(False)
                        else:
                            markers[attribute_name].setHidden(True)
                attribute_y_offset += self.parent.attribute_height + self.parent.animator_margin
            for frame, marker in self.header_markers.items():
                marker.setHidden(True)

    def update_height(self):
        if self.folded:
            self.setFixedHeight(self.parent.attribute_height)
        else:
            if len(self.attributes.keys()) > len(self.hidden_attributes):
                self.setFixedHeight((self.parent.attribute_height + self.parent.animator_margin) * (len(self.attributes.keys()) - len(self.hidden_attributes)))
            else:
                self.setFixedHeight(0)


    def set_name(self, name):
        self.name = name
        self.header.setText('{}\n{}'.format(name.split(' ')[0], ' '.join(name.split(' ')[1:])))

    def add_attribute_marker(self, frame, attribute_name):
        if frame in self.attribute_markers.keys() and attribute_name in self.attribute_markers[frame].keys():
            return
        if frame not in self.attribute_markers: self.attribute_markers[frame] = {}
        self.attribute_markers[frame][attribute_name] = MarkerWidget(0, 0, self.parent.marker_size / 3, self.parent.marker_size)
        self.parent.scene.addWidget(self.attribute_markers[frame][attribute_name])
        self.attribute_markers[frame][attribute_name].setHidden(True)

    def add_header_marker(self, frame):
        if frame in self.header_markers.keys():
            return
        self.header_markers[frame] = MarkerWidget(0, 0, self.parent.marker_size / 3, self.parent.marker_size)
        self.parent.scene.addWidget(self.header_markers[frame])
        self.header_markers[frame].setHidden(True)

    def on_scroll(self):
        self.update_position()

    def on_scale(self):
        self.update_position()

    def update_markers(self):
        markers_to_delete = []
        last_keyframe_for_layer = program_data['manager'].get_last_keyframe_for_layer(program_data['selected_layer'])
        for attribute_name, attribute in self.attributes.items():
            for frame in range(last_keyframe_for_layer):
                frame = str(frame + self.parent.frame_offset)
                if frame in program_data['keyframes'].keys():
                    if self.animator_id in program_data['keyframes'][frame].keys():
                        if attribute_name in program_data['keyframes'][frame][self.animator_id].keys():
                            self.add_attribute_marker(frame, attribute_name)
                        self.add_header_marker(frame)

        for frame, attribute_name, marker in self.get_markers():
            if (frame not in program_data['keyframes'].keys()
                or self.animator_id not in program_data['keyframes'][frame].keys()
                or (attribute_name != 'Header' and attribute_name not in program_data['keyframes'][frame][self.animator_id].keys())):
                markers_to_delete.append((frame, attribute_name))

        for frame, attribute_name in markers_to_delete:
            if attribute_name == 'Header': self.header_markers.pop(frame).deleteLater()
            else:                          self.attribute_markers[frame].pop(attribute_name).deleteLater()

            if self.parent.selection.is_selected(frame, self.animator_id, 'Header'):
                self.parent.selection.deselect(frame, self.animator_id, 'Header')

        self.update_position()

    def on_effect_name_change(self):
        self.hidden_attributes = []
        for frame in list(self.attribute_markers.keys()):
            for attribute_name in list(self.attribute_markers[frame].keys()):
                self.attribute_markers[frame].pop(attribute_name).deleteLater()
        for attribute_name in list(self.attributes.keys()):
            self.attributes.pop(attribute_name).deleteLater()
        self.update_attributes()

    def update_hidden(self):
        for attribute_name in self.attributes:
            if self.animator_id in program_data['keyframe_targets'] and attribute_name in program_data['keyframe_targets'][self.animator_id]:
                if attribute_name in self.hidden_attributes:
                    self.hidden_attributes.remove(attribute_name)
            else:
                if attribute_name not in self.hidden_attributes:
                    self.hidden_attributes.append(attribute_name)


        if len(self.attributes) == len(self.hidden_attributes):
            self.hidden = True
        else:
            self.hidden = False

        if program_data['effects'][self.animator_id]['layer'] != program_data['selected_layer']:
            self.hidden = True

        self.setHidden(self.hidden)

        self.enforce_hidden()

    def enforce_hidden(self):
        self.header.setHidden(self.hidden)

        for attribute_name, attribute in self.attributes.items():
            if self.hidden or attribute_name in self.hidden_attributes:
                attribute.setHidden(True)
            elif not self.folded:
                attribute.setHidden(False)

        for frame, attribute_name, marker in self.get_markers():
            if self.hidden or attribute_name in self.hidden_attributes:
                marker.setHidden(True)
                if self.parent.selection.is_selected(frame, self.animator_id, attribute_name):
                    self.parent.selection.deselect(frame, self.animator_id, attribute_name)
            else:
                if attribute_name == 'Header' and self.folded:
                    marker.setHidden(False)
                elif attribute_name == 'Header' and not self.folded:
                    marker.setHidden(True)
                elif self.folded:
                    marker.setHidden(True)
                else:
                    marker.setHidden(False)

        self.update_height()
        self.update_position()

    def update_attributes(self):
        attributes_to_remove = []
        attribute_y = self.parent.header_height
        if '1' in program_data['keyframes']:
            if self.animator_id in program_data['keyframes']['1'].keys():
                for attribute_name in program_data['keyframes']['1'][self.animator_id]:
                    if attribute_name not in self.attributes:
                        self.attributes[attribute_name] = QW.QLabel(attribute_name)
                        self.attributes[attribute_name].setStyleSheet('font-size:16pt;')
                        self.attributes[attribute_name].setAlignment(Qt.AlignRight)
                        self.attributes[attribute_name].setContentsMargins(2, 0, 2, 0)
                        self.attributes[attribute_name].setFixedWidth(self.width)
                        self.attributes[attribute_name].setFixedHeight(self.parent.attribute_height)
                        self.attributes[attribute_name].move(0, attribute_y)
                        if self.folded or self.hidden or attribute_name in self.hidden_attributes:
                            self.attributes[attribute_name].setHidden(True)
                        self.layout().addWidget(self.attributes[attribute_name])
                        attribute_y += self.parent.attribute_height + self.parent.animator_margin
            else:
                return

            for attribute_name in list(self.attributes.keys()):
                if attribute_name not in program_data['keyframes']['1'][self.animator_id]: self.attributes.pop(attribute_name).deleteLater()
                if attribute_name in self.hidden_attributes:                               self.hidden_attributes.remove(attribute_name)


            self.update_hidden()
            self.update_markers()

    def deleteLater(self, *args, **kwargs):
        for frame, attribute_name, marker in self.get_markers():
            marker.deleteLater()
        super(TimelineAnimatorWidget, self).deleteLater(*args, **kwargs)

import PyQt5.QtWidgets as QW

from PyQt5.QtCore import QRectF

from utils.pos_inside_rect import pos_inside_rect
from utils.min_max import min_max


class TimelineView(QW.QGraphicsView):
    def __init__(self, parent, *args, **kwargs):
        super(TimelineView, self).__init__(*args, **kwargs)
        self.parent = parent

        self.pressing_shift                 = False
        self.pressing_left_mouse            = False
        self.pressing_left_mouse_start_pos  = None
        self.pressing_left_mouse_selected   = []
        self.pressing_right_mouse           = False
        self.pressing_right_mouse_start_pos = None

        self.drag_start_frame = None
        self.drag_start_pos   = None
        self.dragging         = False
        self.drag_selected    = []
        self.drag_margin      = 400  # amount mouse needs to move before initiating dragging

        self.scaling               = False
        self.scale_start_frame     = None
        self.scale_end_frame       = None
        self.scale_start_handle    = None
        self.scale_last_call_frame = None

        self.wheel_velocity_max = 200

        self.moving = False
        self.move_start_frame = None

    def mouseMoveEvent(self, event):
        super(TimelineView, self).mouseMoveEvent(event)
        self.setFocus()
        mouse_position = self.mapToScene(event.pos())
        frame = self.parent.get_frame_from_pos(mouse_position)

        if self.dragging:
            checked_markers = []
            for frame, animator_id, attribute_name in self.parent.get_all_markers_within_rect(self.parent.drag_indicator.rect()):
                checked_markers.append([frame, animator_id, attribute_name])
                if [frame, animator_id, attribute_name] not in self.drag_selected:
                    self.drag_selected.append([frame, animator_id, attribute_name])
                    if self.parent.selection.is_selected(frame, animator_id, attribute_name):
                        if self.pressing_shift:
                            self.parent.selection.deselect(frame, animator_id, attribute_name, fake=True)
                    else:
                        self.parent.selection.select(frame, animator_id, attribute_name, fake=True)
            for frame, animator_id, attribute_name in list(self.drag_selected):
                if [frame, animator_id, attribute_name] not in checked_markers:
                    self.drag_selected.remove([frame, animator_id, attribute_name])
                    self.parent.selection.refresh_marker(frame, animator_id, attribute_name)

        if self.scaling:
            if frame != self.scale_last_call_frame:
                self.parent.selection.scale_selection_markers(self.scale_start_frame, frame)
            self.scale_last_call_frame = frame

        elif not self.moving:
            if self.pressing_left_mouse:
                if (self.pressing_left_mouse_start_pos.x() > mouse_position.x() - self.drag_margin or
                    self.pressing_left_mouse_start_pos.x() < mouse_position.x() + self.drag_margin):

                    self.drag_start_pos = self.pressing_left_mouse_start_pos
                    self.drag_start_frame = frame
                    self.dragging = True
                    self.parent.update_drag()
                    if not self.pressing_shift:
                        self.parent.selection.clear_selection()
            else:
                if not self.parent.selection.left_handle.isHidden():
                    if self.parent.selection.left_handle.rect().contains(mouse_position):
                        self.parent.selection.left_handle.hover()
                    else:
                        self.parent.selection.left_handle.hover(False)
                if not self.parent.selection.right_handle.isHidden():
                    if self.parent.selection.right_handle.rect().contains(mouse_position):
                        self.parent.selection.right_handle.hover()
                    else:
                        self.parent.selection.right_handle.hover(False)

        self.parent.update_mouse_position(mouse_position, frame)

    def mousePressEvent(self, event):
        super(TimelineView, self).mousePressEvent(event)
        mouse_position = self.mapToScene(event.pos())
        frame = self.parent.get_frame_from_pos(mouse_position)
        if self.mapToScene(event.pos()).x() > self.parent.lines_start_position:  # animators handle their own clicks
            if event.button() == 1:
                self.pressing_left_mouse = True
                self.pressing_left_mouse_start_pos = mouse_position
                if not self.parent.selection.left_handle.isHidden() and self.parent.selection.left_handle.rect().contains(mouse_position):
                    self.parent.selection.left_handle.hover(False)
                    self.scaling = True
                    self.scale_start_handle = 'left'
                    self.scale_start_frame = frame
                    self.parent.selection.copy_selection_markers()
                    self.parent.selection.scale_selection_markers(frame, frame)
                    return
                if not self.parent.selection.right_handle.isHidden() and self.parent.selection.right_handle.rect().contains(mouse_position):
                    self.parent.selection.right_handle.hover(False)
                    self.scaling = True
                    self.scale_start_handle = 'right'
                    self.scale_start_frame = frame
                    self.parent.selection.copy_selection_markers()
                    self.parent.selection.scale_selection_markers(frame, frame)
                    return
                for frame, animator_id, attribute_name in self.parent.get_marker_from_pos(self.pressing_left_mouse_start_pos):
                    if not self.parent.selection.is_empty():
                        if not self.parent.selection.is_selected(frame, animator_id, attribute_name):
                            if not self.pressing_shift:
                                self.parent.selection.clear_selection()
                            self.parent.selection.select(frame, animator_id, attribute_name)
                            self.pressing_left_mouse_selected.append([frame, animator_id, attribute_name])
                    else:
                        self.parent.selection.select(frame, animator_id, attribute_name)
                        self.pressing_left_mouse_selected.append([frame, animator_id, attribute_name])
                    self.moving = True
                    self.move_start_frame = self.parent.get_frame_from_pos(event.pos())
                    self.parent.on_shift_pressed()
                    self.parent.update_move()
            elif event.button() == 2:
                for frame, animator_id, attribute_name in self.parent.get_marker_from_pos(event.pos()):
                    self.parent.selection.select(frame, animator_id, attribute_name)
                self.pressing_right_mouse = True
                self.pressing_right_mouse_start_pos = event.screenPos()
                self.parent.selection.start_dialog(event.globalPos())
                # self.parent.on_right_mouse_pressed()

    def mouseReleaseEvent(self, event):
        super(TimelineView, self).mouseReleaseEvent(event)
        mouse_position = self.mapToScene(event.pos())
        frame = self.parent.get_frame_from_pos(mouse_position)
        if event.button() == 1:
            self.pressing_left_mouse = False
            if self.dragging:
                self.dragging = False
                self.drag_selected = []
                self.parent.update_drag()
                self.parent.selection.refresh_selected()
                for frame, animator_id, attribute_name in self.parent.get_all_markers_within_rect(self.parent.drag_indicator.rect()):
                    if self.pressing_shift:
                        if self.parent.selection.is_selected(frame, animator_id, attribute_name):
                            self.parent.selection.deselect(frame, animator_id, attribute_name)
                        else:
                            self.parent.selection.select(frame, animator_id, attribute_name)
                    else:
                        self.parent.selection.select(frame, animator_id, attribute_name)

            elif self.moving:
                self.moving = False
                self.move_end_frame = frame
                self.parent.update_move()
                self.parent.selection.move_selection(self.move_start_frame, self.move_end_frame)
                if self.move_end_frame == self.move_start_frame:
                    for frame, animator_id, attribute_name in self.parent.get_marker_from_pos(self.mapToScene(event.pos())):
                        if not [frame, animator_id, attribute_name] in self.pressing_left_mouse_selected:
                            if not self.pressing_shift:
                                self.parent.selection.clear_selection()
                            if self.pressing_shift:
                                if self.parent.selection.is_selected(frame, animator_id, attribute_name):
                                    self.parent.selection.deselect(frame, animator_id, attribute_name)
                                else:
                                    self.parent.selection.select(frame, animator_id, attribute_name)
                            else:
                                self.parent.selection.select(frame, animator_id, attribute_name)

            elif self.scaling:
                if self.scale_start_handle == 'left' and self.parent.selection.left_handle.rect().contains(mouse_position):
                        self.parent.selection.left_handle.hover()
                elif self.parent.selection.right_handle.rect().contains(mouse_position):
                        self.parent.selection.right_handle.hover()
                        self.right_selection_handle_hovered = True

                self.scaling = False
                self.scale_end_frame = frame
                self.parent.selection.clear_selection_markers()
                self.parent.selection.scale_selection(self.scale_start_frame, self.scale_end_frame)
                self.parent.on_shift_pressed()

            elif self.mapToScene(event.pos()).x() > self.parent.lines_start_position:
                self.parent.frame_clicked(frame)
                if not self.pressing_shift:
                    self.parent.selection.clear_selection()

            self.pressing_left_mouse_selected = []

        elif event.button() == 2:
            self.pressing_right_mouse = False
            self.parent.on_right_mouse_pressed()

    def wheelEvent(self, event):
        velocity = min_max(event.angleDelta().y() * -1,
                           -self.wheel_velocity_max,
                           self.wheel_velocity_max)
        if self.pressing_shift:
            self.parent.scale(velocity / 2)
        else:
            self.parent.scroll(velocity / 3)

    def keyPressEvent(self, event):
        if event.key() == 16777248:
            self.pressing_shift = True
            self.parent.on_shift_pressed()
        super(TimelineView, self).keyPressEvent(event)

    def keyReleaseEvent(self, event):
        if event.key() == 16777248:
            self.pressing_shift = False
            self.parent.on_shift_pressed()
        super(TimelineView, self).keyPressEvent(event)

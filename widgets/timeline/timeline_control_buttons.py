import PyQt5.QtWidgets as QW
from PyQt5.QtCore import pyqtSignal, pyqtSlot

from widgets.icon_button import IconButton

from utils.get_path_as_string import get_path_as_string

from definitions import program_data, GRAPHICS_DIR

GRAPHICS_PLAY_ICON                   = get_path_as_string(GRAPHICS_DIR, 'iconPlay.svg')
GRAPHICS_PAUSE_ICON                  = get_path_as_string(GRAPHICS_DIR, 'iconPause.svg')
GRAPHICS_STOP_ICON                   = get_path_as_string(GRAPHICS_DIR, 'iconStop.svg')
GRAPHICS_STEP_FORWARD_ICON           = get_path_as_string(GRAPHICS_DIR, 'iconStepForwards.svg')
GRAPHICS_STEP_BACKWARD_ICON          = get_path_as_string(GRAPHICS_DIR, 'iconStepBackwards.svg')
GRAPHICS_STEP_FORWARD_FORWARD_ICON   = get_path_as_string(GRAPHICS_DIR, 'iconStepForwardsForwards.svg')
GRAPHICS_STEP_BACKWARD_BACKWARD_ICON = get_path_as_string(GRAPHICS_DIR, 'iconStepBackwardsBackwards.svg')


class TimelineControlButtons(QW.QWidget):
    play = pyqtSignal()
    pause = pyqtSignal()
    stop = pyqtSignal()

    def __init__(self, button_size, *args, **kwargs):
        super(TimelineControlButtons, self).__init__(*args, **kwargs)
        self.setLayout(QW.QHBoxLayout())
        self.layout().setSpacing(6)
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.connect_signals()

        def add_button(icon, fun=None, hidden=False):
            button = IconButton(icon, button_size, button_size)
            self.layout().addWidget(button)
            if fun:
                button.clicked.connect(fun)
            if hidden:
                button.setHidden(True)
            return button

        self.children = {
            'stepBackwardBackwardBtn': add_button(GRAPHICS_STEP_BACKWARD_BACKWARD_ICON,
                                                  self.backwards_backwards_button_clicked),
            'stepBackwardBtn':         add_button(GRAPHICS_STEP_BACKWARD_ICON,
                                                  self.backwards_button_clicked),
            'playBtn':                 add_button(GRAPHICS_PLAY_ICON,
                                                  self.play_button_clicked),
            'pauseBtn':                add_button(GRAPHICS_PAUSE_ICON,
                                                  self.pause_button_clicked, True),
            'stopBtn':                 add_button(GRAPHICS_STOP_ICON,
                                                  self.stop_button_clicked),
            'stepForwardBtn':          add_button(GRAPHICS_STEP_FORWARD_ICON,
                                                  self.forwards_button_clicked),
            'stepForwardForwardBtn':   add_button(GRAPHICS_STEP_FORWARD_FORWARD_ICON,
                                                  self.forwards_forwards_button_clicked),
        }

    def connect_signals(self):
        self.play.connect(program_data['manager'].start_playback)
        self.pause.connect(program_data['manager'].pause_playback)
        self.stop.connect(program_data['manager'].stop_playback)

        program_data['signals'].playback_playing_changed.connect(self.update_playing)

    def play_button_clicked(self):
        self.play.emit()

    def pause_button_clicked(self):
        self.pause.emit()

    def stop_button_clicked(self):
        self.stop.emit()

    def backwards_button_clicked(self):
        if program_data['selected_frame'] == '1':
            program_data['manager'].select_keyframe(str(program_data['manager'].get_last_keyframe()))
        else:
            frame_to_select = 1
            for frame in program_data['keyframes']:
                if int(frame) < int(program_data['selected_frame']) and int(frame) > frame_to_select:
                    frame_to_select = int(frame)
            program_data['manager'].select_keyframe(str(frame_to_select))

    def backwards_backwards_button_clicked(self):
        program_data['manager'].select_keyframe('1')

    def forwards_button_clicked(self):
        if program_data['selected_frame'] == str(program_data['manager'].get_last_keyframe()):
            program_data['manager'].select_keyframe('1')
        else:
            frame_to_select = program_data['manager'].get_last_keyframe()
            for frame in program_data['keyframes']:
                if int(frame) > int(program_data['selected_frame']) and int(frame) < frame_to_select:
                    frame_to_select = int(frame)
            program_data['manager'].select_keyframe(str(frame_to_select))

    def forwards_forwards_button_clicked(self):
        program_data['manager'].select_keyframe(str(program_data['manager'].get_last_keyframe()))

    @pyqtSlot(bool)
    def update_playing(self, playing):
        self.children['playBtn'].setHidden(True)
        self.children['pauseBtn'].setHidden(True)
        if playing:
            self.children['pauseBtn'].setHidden(False)
        else:
            self.children['playBtn'].setHidden(False)

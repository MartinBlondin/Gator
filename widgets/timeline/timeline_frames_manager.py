import PyQt5.QtWidgets as QW

from widgets.rect_widget import RectWidget

from definitions import program_data, COLORS


class TimelineFramesManager:
    def __init__(self, parent, x, *args, **kwargs):
        super(TimelineFramesManager, self).__init__(*args, **kwargs)
        self.parent = parent
        self.x_offset = x + 10
        self.colors = {'inactive':  COLORS['background_dark'],
                       'incomplete': COLORS['timeline_frame_tint_incomplete'],
                       'complete':  COLORS['timeline_frame_tint_complete']}
        self.frames = {}
        self.hidden_frames = []
        self.draw_frames()
        self.connect_signals()

    def connect_signals(self):
        program_data['signals'].layer_selected.connect(self.update_states)
        program_data['signals'].preview_changed.connect(self.set_frame_state)

        program_data['signals'].keyframe_added.connect(self.update_states)
        program_data['signals'].keyframe_changed.connect(self.update_states)
        program_data['signals'].keyframe_deleted.connect(self.update_states)

    def on_scroll(self):
        self.draw_frames()

    def on_scale(self):
        self.draw_frames()

    def update_states(self):
        for frame, frame_dict in self.frames.items():
            if frame not in self.hidden_frames:
                frame += self.parent.frame_offset
                try:
                    state = program_data['preview_images'][str(frame)][program_data['selected_layer']]['status']
                except KeyError:
                    state = 'inactive'

                frame_dict['state'] = state
                frame_dict['widget'].setColor(self.colors[state])

    def draw_frames(self):
        frame = -1
        x = self.x_offset
        while x < self.parent.visible_area.width():
            frame += 1
            if frame not in self.frames.keys():
                self.frames[frame] = {'widget': RectWidget(self.colors['inactive'], x, self.parent.visible_area.y(),
                                                           self.parent.frame_distance, self.parent.visible_area.height(),
                                                           0.02),
                                      'state': 'inactive'}
                self.parent.scene.addItem(self.frames[frame]['widget'])
            else:
                self.frames[frame]['widget'].move(x, self.parent.visible_area.y())
                self.frames[frame]['widget'].setFixedWidth(self.parent.frame_distance)
                self.frames[frame]['widget'].setHidden(False)
                if frame in self.hidden_frames:
                    self.hidden_frames.remove(frame)
            x += self.parent.frame_distance

        last_visible_frame = frame

        for frame, frame_dict in self.frames.items():
            if frame > last_visible_frame:
                frame_dict['widget'].setHidden(True)
                self.hidden_frames.append(frame)

        self.update_states()

    def set_frame_state(self, frame, layer_id):
        if layer_id == program_data['selected_layer'] or layer_id == None:
            if frame not in self.hidden_frames:
                try:
                    state = program_data['preview_images'][frame][program_data['selected_layer']]['status']
                except KeyError:
                    state = 'inactive'

                frame = int(frame) - self.parent.frame_offset
                if frame in self.frames and frame >= 0:
                    self.frames[frame]['state'] = state
                    self.frames[frame]['widget'].setColor(self.colors[state])

from widgets.editors.base_effect_editor import BaseEffectEditor

class GmicEffectEditor(BaseEffectEditor):
    effect_prefix = 'gmic'

from PyQt5.QtGui import QPolygonF, QPainterPath, QColor, QPainter
from PyQt5.QtCore import QPointF, Qt
import PyQt5.QtWidgets as QW

from widgets.editors.base_module_editor import BaseModuleEditor
from widgets.category_combo_box import CategoryComboBox
from widgets.wave_display import WaveDisplay

from definitions import program_data, GENERATOR_SIGNALS, COLORS


class BaseGeneratorEditor(BaseModuleEditor):
    def __init__(self, parent, generator_id, controller, *args, **kwargs):
        super(BaseGeneratorEditor, self).__init__(parent, generator_id, controller, *args, **kwargs)

    def module_specific_init_ui(self):
        generator_combo_box = CategoryComboBox(width=150, height=30, pos_type='top')
        self.children['moduleToolbar'].layout().replaceWidget(self.children['moduleComboBox'], generator_combo_box)
        self.children['moduleComboBox'].deleteLater()
        self.children['moduleComboBox'] = generator_combo_box

        self.children['waveDisplay'] = WaveDisplay(self.module_id)
        self.children['attributeSettingsContainer'].layout().addWidget(self.children['waveDisplay'])

    def connect_signals(self):
        super(BaseGeneratorEditor, self).connect_signals()

        self.name_change.connect(program_data['manager'].change_generator_type)
        self.delete_module.connect(program_data['manager'].delete_generator)

        program_data['signals'].generator_type_changed.connect(self.set_name_combo_box)
        program_data['signals'].keyframe_changed.connect(self.keyframe_changed)

    def keyframe_changed(self, frame, animator_id, attribute_name, attribute_value, animator_type):
        if animator_id == self.module_id and attribute_name[0] != 'C':
            self.children['waveDisplay'].update_wave()

    def setup_combo_box(self):
        self.children['moduleComboBox'].addItem('Oscillator')
        self.children['moduleComboBox'].setCurrentText('Oscillator')

    def get_attribute_value_at_frame(self, frame, attribute_name):
        generator = program_data['generators'][self.module_id]
        if (frame in program_data['render_states'] and
            self.module_id in program_data['render_states'][frame]):
            attribute_value = program_data['render_states'][frame][layer_id][0][self.module_id][attribute_name]
            if generator['attributes'][attribute_name]['type'].split(':')[0] == 'position': attribute_value *= 0.01
        else:
            attribute_value = program_data['manager'].get_attribute_value_at_frame(frame, self.module_id, attribute_name)
        return attribute_value

    def on_name_changed(self, generator_name):
        if self.emit_name_change:
            self.name_change.emit(self.module_id, '{}.{}'.format('Base', generator_name))

    def get_attributes(self):
        return program_data['generators'][self.module_id]['attributes']

import PyQt5.QtWidgets as QW
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtGui import QCursor

from utils.min_max import min_max

from widgets.base_combo_box_dialog import BaseComboBoxDialog


class BaseAttributeEditor(QW.QWidget):
    valueChanged = pyqtSignal(object)

    def __init__(self):
        super(BaseAttributeEditor, self).__init__()
        self.pressing_mouse = False
        self.default = None
        self.setLayout(QW.QVBoxLayout())
        self.attribute_dialog = BaseComboBoxDialog(hide_verical_scrollbar=False)
        self.attribute_dialog.add_option('Reset')
        self.attribute_dialog.option_selected_changed.connect(self.reset_to_default)
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.layout().setSpacing(0)

    def set_default(self, value):
        self.default = value

    def reset_to_default(self):
        self.setValue(self.default)

    def start_dialog(self):
        if self.default:
            self.attribute_dialog.start(QCursor().pos())

    def on_mouse_press(self, button, pos):
        if button == 1:
            self.pressing_mouse = True
            self.widget.update_mouse(pos)
        elif button == 2:
            self.start_dialog()

    def on_mouse_move(self, button, pos):
        if self.pressing_mouse:
            self.widget.update_mouse(pos)

    def on_mouse_release(self, button, pos):
        if button == 1:
            self.pressing_mouse = False
            self.widget.update_mouse(pos)
            self.valueChanged.emit(self.widget.value())

    def setMaximum(self, value): self.widget.setMaximum(value)
    def setMinimum(self, value): self.widget.setMinimum(value)
    def setValue(self, *args, **kwargs): self.widget.setValue(*args, **kwargs)
    def update_mouse(self, pos): pass

from PyQt5.QtWidgets import QGraphicsView, QGraphicsScene, QGraphicsItem
from PyQt5.QtSvg import QSvgWidget
from PyQt5.QtCore import Qt, QRect, QPoint, pyqtSignal

from widgets.rect_widget import RectWidget
from widgets.editors.base_attribute_editor import BaseAttributeEditor

from utils.pos_inside_rect import pos_inside_rect

from definitions import COLORS, POSITION_EDITOR_CIRCLE_PATH, POSITION_EDITOR_HIGHLIGHT_PATH, POSITION_EDITOR_HOVER_PATH


class PositionAttributeEditor(BaseAttributeEditor):
    def __init__(self, effectData, width, *args, **kwargs):
        super(PositionAttributeEditor, self).__init__(*args, **kwargs)
        self.widget = PositionAttributeEditorWidget(self, effectData, width)
        self.layout().addWidget(self.widget)
        self.setMaximumHeight(self.widget.height())

    def reset_to_default(self):
        self.widget.setValue(*self.widget.default)


class PositionAttributeEditorWidget(QGraphicsView):
    def __init__(self, parent, effectData, width, *args, **kwargs):
        super(PositionAttributeEditorWidget, self).__init__(*args, **kwargs)
        self.graphicsScene = QGraphicsScene()
        self.setScene(self.graphicsScene)
        self.setFocusPolicy(Qt.NoFocus)
        self.pressing_mouse = False
        self.parent = parent
        self.setStyleSheet('background-color: transparent;')

        self.setFixedWidth(width)
        self.setFixedHeight(width / 2)
        self.setMaximumSize(self.width(), self.height())
        self.setMinimumSize(self.width(), self.height())

        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        self.background = RectWidget(COLORS['position_editor_background'], 0, 0, self.width(), self.height(), bradius=3)
        self.graphicsScene.addItem(self.background)

        self.circle = QSvgWidget(POSITION_EDITOR_CIRCLE_PATH)
        self.circle.setStyleSheet('background-color: transparent;')
        self.graphicsScene.addWidget(self.circle)
        self.circle.move((self.background.rect().width() / 2) - (self.circle.width() / 2),
                         (self.background.rect().height() / 2) - (self.circle.height() / 2))

        self.highlight_circle = QSvgWidget(POSITION_EDITOR_HIGHLIGHT_PATH)
        self.highlight_circle.setStyleSheet('background-color: transparent;')
        self.graphicsScene.addWidget(self.highlight_circle)
        self.highlight_circle.move(self.circle.pos())
        self.highlight_circle.setHidden(True)

        self.hover_circle = QSvgWidget(POSITION_EDITOR_HOVER_PATH)
        self.hover_circle.setStyleSheet('background-color: transparent;')
        self.graphicsScene.addWidget(self.hover_circle)
        self.hover_circle.move((self.background.rect().width() / 2) - (self.hover_circle.width() / 2),
                              (self.background.rect().height() / 2) - (self.hover_circle.height() / 2))
        self.hover_circle.setHidden(True)

        self.x = effectData['value']
        self.y = effectData['value']
        self.default = [self.x, self.y]

    def setValue(self, x, y=None, target=None):
        if not self.pressing_mouse:
            if target:
                if target == 'x':   self.x = x
                elif target == 'y': self.y = x  # its more readable this way i promise
            else:
                self.x = x
                self.y = y

            self.move_circle(self.x * self.background.rect().width(), self.y * self.background.rect().height())

    def move_circle(self, x, y):
        if x > self.background.rect().width(): x = self.background.rect().width()
        elif x <= 0:                    x = 0

        if y > self.background.rect().height(): y = self.background.rect().height()
        elif y <= 0:                     y = 0

        x, y = float(x), float(y)
        self.circle.move(x - (self.circle.width() / 2),
                         y - (self.circle.height() / 2))
        self.highlight_circle.move(self.circle.pos())
        self.hover_circle.move(x - (self.hover_circle.width() / 2),
                              y - (self.hover_circle.height() / 2))

        self.setSceneRect(self.background.rect().x(),
                          self.background.rect().y(),
                          self.background.rect().width(),
                          self.background.rect().height())

    def update_value(self):
        margin = self.circle.width() / 2
        self.x = (self.circle.x() + margin) / self.background.rect().width()
        self.y = (self.circle.y() + margin) / self.background.rect().height()

    def mouseMoveEvent(self, event):
        super(PositionAttributeEditorWidget, self).mouseMoveEvent(event)
        if pos_inside_rect(event.pos(), self.circle, margin=30):

            self.highlight_circle.setHidden(False)

            if pos_inside_rect(event.pos(), self.circle):
                self.hover_circle.setHidden(False)
            else:
                self.hover_circle.setHidden(True)

            if self.pressing_mouse:
                self.highlight_circle.setHidden(True)
                self.move_circle(event.x(), event.y())
                self.update_value()
        else:
            self.highlight_circle.setHidden(True)

    def mousePressEvent(self, event):
        super(PositionAttributeEditorWidget, self).mousePressEvent(event)
        if event.button() == 1:
            self.pressing_mouse = True
            self.highlight_circle.setHidden(True)
            self.hover_circle.setHidden(False)
            self.move_circle(event.x(), event.y())

    def mouseReleaseEvent(self, event):
        super(PositionAttributeEditorWidget, self).mouseReleaseEvent(event)
        if event.button() == 1:
            self.pressing_mouse = False
            self.highlight_circle.setHidden(False)

            self.update_value()
            self.parent.valueChanged.emit((self.x, self.y))

        elif event.button() == 2:
            self.parent.start_dialog()

import PyQt5.QtWidgets as QW
from PyQt5.QtSvg import QSvgWidget
from PyQt5.QtCore import Qt, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QCursor

from pathlib import Path

from widgets.icon_button import IconButton
from widgets.combo_box import ComboBox

from widgets.editors.position_attribute_editor import PositionAttributeEditor
from widgets.editors.slider_attribute_editor import SliderAttributeEditor
from widgets.editors.combo_attribute_editor import ComboAttributeEditor

from widgets.qmlComponents.ModuleEditor import Ui_ModuleEditor

from utils.get_path_as_string import get_path_as_string

from definitions import program_data, GRAPHICS_DIR, DELETE_ICON_PATH, KEYFRAME_ICON_PATH, KEYFRAME_SELECTED_ICON_PATH, KEYFRAME_EMPTY_ICON_PATH, SINE_ICON_PATH, COLORS


class BaseModuleEditor(QW.QWidget):
    name_change            = pyqtSignal(str, str)
    delete_module          = pyqtSignal(str)
    attribute_value_change = pyqtSignal(str, str, str, object, str)
    add_keyframe_target    = pyqtSignal(str, str)
    delete_keyframe_target = pyqtSignal(str, str)

    def __init__(self, parent, module_id, controller, *args, **kwargs):
        super(BaseModuleEditor, self).__init__(parent, *args, **kwargs)
        self.parent     = parent
        self.module_id  = module_id
        self.controller = controller

        self.attribute_editors = {}
        self.keyframe_buttons  = {}
        self.markers           = {}
        self.labels            = {}
        self.types             = {}

        self.ignored_attributes = []
        self.widgets            = []

        self.emit_attribute_changes = True
        self.emit_name_change       = True
        self.emit_marker_toggles    = True

        self.hidden = False

        self.init_ui()
        self.connect_signals()

    def connect_signals(self):
        self.children['moduleComboBox'].currentTextChanged.connect(self.on_name_changed)
        self.children['deleteBtn'].clicked.connect(self.delete_module_button_clicked)

        self.attribute_value_change.connect(program_data['manager'].add_keyframe)
        self.add_keyframe_target.connect(program_data['manager'].add_keyframe_target)
        self.delete_keyframe_target.connect(program_data['manager'].delete_keyframe_target)

        program_data['signals'].keyframe_changed.connect(self.set_attribute)
        program_data['signals'].keyframe_added.connect(self.set_attribute)
        program_data['signals'].keyframe_selected.connect(self.update_values)
        program_data['signals'].playback_frame_changed.connect(self.update_values)
        program_data['signals'].keyframe_target_added.connect(self.on_keyframe_target_added)
        program_data['signals'].keyframe_target_deleted.connect(self.on_keyframe_target_deleted)

        program_data['signals'].window_size_changed.connect(self.on_resize)

    def init_ui(self):
        Ui_ModuleEditor().setupUi(self)

        self.children = {
            'attributeSettingsContainer': self.findChild(QW.QWidget, 'attributeSettingsContainer'),
            'scrollAreaContainer':        self.findChild(QW.QWidget, 'scrollAreaContainer'),
            'scrollArea':                 self.findChild(QW.QScrollArea, 'scrollArea'),
            'moduleToolbar':              self.findChild(QW.QWidget, 'moduleToolbar'),
            'deleteBtn':                  IconButton(DELETE_ICON_PATH, 30, 30)}
        self.children['attributeSettingsContainer'].setLayout(QW.QVBoxLayout())
        self.children['moduleComboBox'] = ComboBox(width=150, height=30, pos_type='top')
        self.children['moduleToolbar'].layout().setContentsMargins(6, 0, 2, 0)
        self.children['moduleToolbar'].layout().addWidget(self.children['moduleComboBox'])
        self.children['moduleToolbar'].layout().addStretch()
        self.children['moduleToolbar'].layout().addWidget(self.children['deleteBtn'])

        self.module_specific_init_ui()

        self.setup_combo_box()

        self.setup_attribute_editors()

    def setup_attribute_editor(self, attr, key, width, height):
        editor_height = 40
        label_text = key
        attribute_editor = SliderAttributeEditor(attr)
        attribute_value_changed_signal = None
        attribute_editor.setMaximumWidth(width)
        attribute_editor.setMinimumWidth(width)
        attribute_editor.setMinimumHeight(editor_height)
        attribute_editor.setMaximumHeight(editor_height)
        attribute_editor.setFocusPolicy(Qt.ClickFocus)
        if attr['type'] == 'int':
            attribute_editor.setMaximum(attr['max'])
            attribute_editor.setMinimum(attr['min'])
            attribute_editor.setValue(attr['value'])
            attribute_editor.set_default(attr['value'])

        elif attr['type'] == 'float':
            attribute_editor.setMaximum(attr['max'] * 100)
            attribute_editor.setMinimum(attr['min'] * 100)
            attribute_editor.setValue(attr['value'] * 100)
            attribute_editor.set_default(attr['value'] * 100)

        elif attr['type'] == 'combo':
            attribute_editor = ComboAttributeEditor(height=editor_height, width=228)
            attribute_value_changed_signal = attribute_editor.currentIndexChanged
            for option in attr['options']:
                attribute_editor.addItem(option)
            attribute_editor.setCurrentIndex(attr['value'])
            attribute_editor.set_default(attr['value'])
            attribute_editor.setMinimumHeight(editor_height)
            attribute_editor.setMaximumHeight(editor_height)

        elif attr['type'].split(':')[0] == 'position':
            label_text = key[:-1]
            self.ignored_attributes.append(attr['link'])
            attribute_editor = PositionAttributeEditor(attr, width)
            self.attribute_editors[attr['link']] = attribute_editor
            attribute_editor.set_default(attr['value'])

        if not attribute_value_changed_signal:
            attribute_value_changed_signal = attribute_editor.valueChanged
        attribute_value_changed_signal.connect(self.attribute_value_changed_middleman)

        label = QW.QLabel()
        label.setFixedHeight(height)
        label.setAlignment(Qt.AlignCenter)
        label.setText(label_text)
        label.setStyleSheet('background-color:transparent;color:{};'.format(COLORS['foreground_dark']))
        return label, attribute_editor

    def setup_attribute_editors(self):
        self.ignored_attributes = []
        container_height = 0
        for key, attr in self.get_attributes().items():
            if attr['type'] == 'invisible':
                self.ignored_attributes.append(key)
            elif key not in self.ignored_attributes:
                layout = QW.QVBoxLayout()
                self.children['attributeSettingsContainer'].layout().addLayout(layout)

                height = 24
                width = 227

                title_layout = QW.QHBoxLayout()
                title_layout.setContentsMargins(0, 0, 2, 0)

                label, attribute_editor = self.setup_attribute_editor(attr, key, width, height)
                self.labels[key] = label

                if isinstance(attribute_editor.height, float) or isinstance(attribute_editor.height, int):
                    container_height += attribute_editor.height
                else:
                    container_height += attribute_editor.height()
                container_height += label.height()
                container_height += height
                attribute_editor.setObjectName(key)

                marker = IconButton(KEYFRAME_EMPTY_ICON_PATH, 26, 28, hide_background=True,
                                    data=key, toggle=True, toggled_icon_path=KEYFRAME_ICON_PATH)
                marker.setContentsMargins(0, 0, 0, 0)
                self.markers[key] = marker
                marker.toggled.connect(self.on_marker_toggled)

                keyframe_settings_button = IconButton(SINE_ICON_PATH, 30, 24, data=key)
                keyframe_settings_button.clicked.connect(self.on_keyframe_settings_button_clicked)
                self.keyframe_buttons[key] = keyframe_settings_button
                keyframe_settings_button.hide(True)

                title_layout.addWidget(marker)
                title_layout.addStretch()
                title_layout.addWidget(label)
                title_layout.addStretch()
                title_layout.addWidget(keyframe_settings_button)
                layout.addLayout(title_layout)
                layout.addWidget(attribute_editor)

                self.widgets.append(layout)
                self.widgets.append(label)
                self.attribute_editors[key] = attribute_editor

        self.children['attributeSettingsContainer'].setMinimumHeight(container_height)
        self.children['attributeSettingsContainer'].setMaximumHeight(container_height)
        self.children['scrollAreaContainer'].setFixedHeight(container_height)
        self.on_resize()

    def on_name_changed(self, module_name):
        if self.emit_name_change:
            self.name_change.emit(self.module_id, module_name)

    def delete_module_button_clicked(self):
        self.delete_module.emit(self.module_id)

    def attribute_value_changed_middleman(self, attribute_value):
        if self.emit_attribute_changes:
            attribute_name = self.sender().objectName()
            self.attribute_value_changed(attribute_name, attribute_value)

    def attribute_value_changed(self, attribute_name, attribute_value):
        if self.emit_attribute_changes:
            if self.get_attributes()[attribute_name]['type'] == 'float':
                attribute_value /= 100

            self.markers[attribute_name].setHidden(False)

            frame = program_data['selected_frame']
            if self.module_id not in program_data['keyframe_targets'] or attribute_name not in program_data['keyframe_targets'][self.module_id]:
                frame = '1'
            self.attribute_value_change.emit(frame, self.module_id, attribute_name, attribute_value, program_data['keyframe_animator_types'][self.module_id])

    def clear_editor(self):
        [item.deleteLater() for item in self.widgets]
        [item.deleteLater() for __, item in self.labels.items()]
        [item.deleteLater() for __, item in self.attribute_editors.items()]
        [item.deleteLater() for __, item in self.markers.items()]
        [item.deleteLater() for __, item in self.keyframe_buttons.items()]
        self.widgets = []; self.attribute_editors = {}
        self.markers = {}; self.keyframe_buttons = {}
        self.labels = {}

    def on_keyframe_settings_button_clicked(self):
        attribute_name = self.sender().data
        self.parent.show_animator_keyframe_settings_dialog(QCursor.pos(), self.module_id, attribute_name)

    def on_marker_toggled(self, toggle_state):
        if self.emit_marker_toggles:
            attribute_name = self.sender().data
            link = None
            if self.get_attributes()[attribute_name]['type'].split(':')[0] == 'position':
                link = self.get_attributes()[attribute_name]['link']
            if toggle_state:
                self.add_keyframe_target.emit(self.module_id, attribute_name)
                if link:
                    self.add_keyframe_target.emit(self.module_id, link)
            else:
                self.delete_keyframe_target.emit(self.module_id, attribute_name)
                if link:
                    self.delete_keyframe_target.emit(self.module_id, link)

    @pyqtSlot(str, str)
    def on_keyframe_target_added(self, animator_id, attribute_name):
        if animator_id == self.module_id and attribute_name not in self.ignored_attributes:
            self.keyframe_buttons[attribute_name].hide(False)
            self.emit_marker_toggles = False
            self.markers[attribute_name].set_toggled(True)
            self.labels[attribute_name].setStyleSheet('background-color:transparent;color:{};'.format(COLORS['foreground_light']))
            self.emit_marker_toggles = True
            self.emit_attribute_changes = False
            frame = program_data['selected_frame']
            self.set_attribute(frame, animator_id, attribute_name, self.get_attribute_value_at_frame(frame, attribute_name), pass_test=True)
            self.emit_attribute_changes = True

    @pyqtSlot(str, object)
    def on_keyframe_target_deleted(self, animator_id, attribute_name):
        if animator_id == self.module_id and attribute_name not in self.ignored_attributes:
            if attribute_name:
                self.keyframe_buttons[attribute_name].hide(True)
                self.emit_marker_toggles = False
                self.markers[attribute_name].set_toggled(False)
                self.labels[attribute_name].setStyleSheet('background-color:transparent;color:{};'.format(COLORS['foreground_dark']))
                frame = program_data['selected_frame']
                self.set_attribute(frame, animator_id, attribute_name, self.get_attribute_value_at_frame(frame, attribute_name), pass_test=True)
                self.emit_marker_toggles = True
            else:
                for attribute_name, button in self.keyframe_buttons.items(): button.hide(True)
                self.emit_marker_toggles = False
                for attribute_name, marker in self.markers.items(): button.set_toggled(False)
                for attribute_name, label in self.labels.items(): label.setStyleSheet('background-color:transparent;color:{};'.format(COLORS['foreground_dark']))
                self.emit_marker_toggles = True

    @pyqtSlot(int, int)
    def on_resize(self, *args):
        self.setFixedHeight(self.parent.height())

    @pyqtSlot(str, str)
    def set_name_combo_box(self, module_id, module_name):
        if module_id == self.module_id:
            self.emit_name_change = False
            self.children['moduleComboBox'].setCurrentText(module_name.split('.')[1])
            self.clear_editor()
            self.setup_attribute_editors()
            self.emit_name_change = True

    @pyqtSlot(str)
    def update_values(self, frame):
        if not self.controller.hidden or not self.hidden:
            for attribute_name in self.attribute_editors:
                self.set_attribute(frame, self.module_id, attribute_name, self.get_attribute_value_at_frame(frame, attribute_name), pass_test=True)

    def get_attribute_value_at_frame(self, frame, attribute_name):
        return program_data['manager'].get_attribute_value_at_frame(frame, self.module_id, attribute_name)

    @pyqtSlot(str, str, str, object)
    def set_attribute(self, frame, module_id, attribute_name, attribute_value, pass_test=False):
        if pass_test or (frame == program_data['selected_frame']
                         and module_id == self.module_id
                         and attribute_name in self.attribute_editors.keys()):
            self.emit_attribute_changes = False

            attribute_info = self.get_attributes()[attribute_name]

            if attribute_info['type'] == 'combo':
                self.attribute_editors[attribute_name].setCurrentIndex(attribute_value)

            elif attribute_info['type'] == 'float':
                self.attribute_editors[attribute_name].setValue(attribute_value * 100)

            elif attribute_info['type'].split(':')[0] == 'position':
                self.attribute_editors[attribute_name].setValue(attribute_value, target=attribute_name[-1])

            else:
                self.attribute_editors[attribute_name].setValue(attribute_value)

            self.emit_attribute_changes = True

    def setHidden(self, hidden):
        super(BaseModuleEditor, self).setHidden(hidden)
        self.hidden = hidden

    def disable(self): [editor.setEnabled(False) for __, editor in self.attribute_editors.items()]
    def enable(self):  [editor.setEnabled(True)  for __, editor in self.attribute_editors.items()]

    def module_specific_init_ui(self): pass

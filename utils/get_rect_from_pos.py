from PyQt5.QtCore import QRect

def get_rect_from_pos(pos1, pos2, rect_type=QRect):
    if pos1.x() > pos2.x():
        width = pos1.x() - pos2.x()
        x = pos2.x()
    else:
        width = pos2.x() - pos1.x()
        x = pos1.x()

    if pos1.y() > pos2.y():
        height = pos1.y() - pos2.y()
        y = pos2.y()
    else:
        height = pos2.y() - pos1.y()
        y = pos1.y()

    return rect_type(x, y, width, height)

import PyQt5.QtWidgets as QW

from PyQt5.QtGui import QGuiApplication


class DebugButtons(QW.QWidget):
    def __init__(self, style=None):
        super(DebugButtons, self).__init__()
        self.show()
        self.setLayout(QW.QVBoxLayout())
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.layout().setSpacing(0)
        self.buttons = []
        self.button_width  = 100
        self.button_height = 50
        self.setFixedWidth(self.button_width)

        if style: self.setStyleSheet(style)
        screen_size = QGuiApplication.primaryScreen().geometry()
        self.move(screen_size.width() - self.button_width, 0)

        self.update_geometry()

    def add_button(self, label, function):
        self.buttons.append(QW.QPushButton(label))
        self.layout().addWidget(self.buttons[-1])
        self.buttons[-1].clicked.connect(function)
        self.buttons[-1].setFixedSize(self.button_width, self.button_height)
        self.update_geometry()

    def update_geometry(self):
        self.setFixedHeight(self.button_height * len(self.buttons))

    def close(self):
        self.deleteLater()

from PyQt5.QtCore import QTimer

def delay(function, time):
    timer = QTimer()
    timer.timeout.connect(function)
    timer.setInterval(time)
    timer.start()
    return timer

def delay_delay(timer, time):
    time -= timer.remainingTime()
    timer.setInterval(timer.interval() + time)

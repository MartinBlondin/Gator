import os
from subprocess import Popen

def run_command(command):
    with open(os.devnull, 'w') as shutup:
        # p = Popen(command, stdout=shutup)
        p = Popen(command, stdout=shutup, stderr=shutup)
        p.wait()

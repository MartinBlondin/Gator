from PyQt5.QtCore import QRect, QRectF, QPointF

def pos_inside_rect(pos, rect, margin=0):
    dimensions = [rect.x() - (margin / 2),
                  rect.y() - (margin / 2),
                  rect.width() + margin,
                  rect.height() + margin]

    if isinstance(pos, QPointF):
        rect = QRectF(*dimensions)
    else:
        rect = QRect(*dimensions)

    return rect.contains(pos)

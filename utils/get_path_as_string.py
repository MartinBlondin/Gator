from pathlib import Path

def get_path_as_string(*args, **kwargs):
    return str(Path(*args, **kwargs).resolve())

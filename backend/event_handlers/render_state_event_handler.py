from backend.event_handlers.base_event_handler import BaseEventHandler

from definitions import program_data


class RenderStateEventHandler(BaseEventHandler):
    def __init__(self, parent):
        self.blocked_signals = [
            'keyframe_selected',
            'preview_changed',
            'playback_frame_changed',
            'playback_playing_changed',
            'history_changed',
            'layer_renamed'
            'layer_selected'
            'shutdown', 'clear'
            'window_changed',
            'window_size_changed',
            'render_state_changed'
        ]
        super(RenderStateEventHandler, self).__init__(parent)

    def on_event(self, *args):
        self.parent.send_update_signal()

    def on_keyframes_added(self, count):
        if not self.catch_ignored_signal('keyframes_added'):
            self.add_ignored_signal('keyframe_added', count)
        else:
            self.add_ignored_signal('keyframe_added', count - 1)

    def on_keyframe_added(self):
        if not self.catch_ignored_signal('keyframe_added'): return
        self.parent.send_update_signal()

    def on_keyframe_target_deleted(self):
        self.on_keyframe_deleted()

    def on_keyframe_deleted(self):
        if not self.catch_ignored_signal('keyframe_deleted'): return

        self.parent.send_update_signal()
        last_keyframe = program_data['manager'].get_last_keyframe()
        for frame in list(program_data['render_states']):
            if int(frame) > last_keyframe:
                for layer_id in program_data['layers']:
                    if layer_id in program_data['render_states'][frame]:
                        program_data['render_states'][frame].pop(layer_id)
                        program_data['manager'].emit(program_data['signals'].render_state_changed, [frame, layer_id, None])
                program_data['render_states'].pop(frame)
        for layer_id in program_data['layers']:
            last_keyframe_for_layer = program_data['manager'].get_last_keyframe_for_layer(layer_id)
            for frame in list(program_data['render_states']):
                if int(frame) > last_keyframe_for_layer and layer_id in program_data['render_states'][frame]:
                    program_data['render_states'][frame].pop(layer_id)
                    program_data['manager'].emit(program_data['signals'].render_state_changed, [frame, layer_id, None])
                    if not program_data['render_states'][frame]:
                        program_data['render_states'].pop(frame)

    def on_keyframes_deleting(self, count):
        if not self.catch_ignored_signal('keyframes_deleted'):
            self.add_ignored_signal('keyframe_deleted', count)
        else:
            self.add_ignored_signal('keyframe_deleted', count - 1)

    def on_layer_deleted(self, layer_id):
        self.parent.send_update_signal()
        for frame in list(program_data['render_states']):
            if layer_id in program_data['render_states'][frame]:
                program_data['render_states'][frame].pop(layer_id)
                if not program_data['render_states'][frame]:
                    program_data['render_states'].pop(frame)

    def on_effect_deleted(self, effect_id):
        for frame in list(program_data['render_states']):
            for layer_id in list(program_data['render_states'][frame]):
                if effect_id in program_data['render_states'][frame][layer_id][0]:
                    render_state = program_data['render_states'][frame][layer_id]
                    render_state[0].pop(effect_id)
                    program_data['manager'].emit(program_data['signals'].render_state_changed, [frame, layer_id, render_state])

    def on_effect_name_changed(self, effect_id):
        self.add_ignored_signal('keyframes_added', 1)
        self.add_ignored_signal('keyframes_deleted', 1)

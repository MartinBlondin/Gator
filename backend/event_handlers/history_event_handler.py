from backend.event_handlers.base_event_handler import BaseEventHandler

from definitions import program_data, EFFECTS


class HistoryEventHandler(BaseEventHandler):
    def __init__(self, parent):
        super(HistoryEventHandler, self).__init__(parent)

    def on_effect_added(self, effect_id, queue_order):
        effect = program_data['effects'][effect_id]
        change = self.parent.register_change('add_effect',
                                             {'effect_id': effect_id,
                                              'effect_name': effect['name'],
                                              'layer_id': effect['layer'],
                                              'queue_order': queue_order,
                                              'keyframe_amount': program_data['manager'].get_amount_of_frames(effect_id),
                                              'attribute_amount': len(EFFECTS[effect['name']]['attributes'])})
        self.parent.add_side_effect('side_effect_keyframes_added', change, 1, 'ignore')

    def on_effect_deleted(self, effect_id, effect_name, layer_id):
        change = self.parent.register_change('delete_effect',
                                             {'effect_id': effect_id,
                                              'effect_name': effect_name,
                                              'layer_id': layer_id,
                                              'queue_order': None,
                                              'keyframe_amount': program_data['manager'].get_amount_of_frames(effect_id),
                                              'attribute_amount': len(EFFECTS[effect_name]['attributes'])})
        self.parent.add_side_effect('side_effect_keyframes_deleted', change, 1, 'append')

    def on_keyframe_added(self, frame, animator_id, attribute_name, attribute_value, animator_type):
        change = self.parent.register_change('add_keyframe',
                                             {'frame':           frame,
                                              'animator_id':     animator_id,
                                              'attribute_name':  attribute_name,
                                              'attribute_value': attribute_value,
                                              'animator_type'  : animator_type})

    def on_keyframe_deleted(self, frame, animator_id, attribute_name, attribute_value, animator_type):
        change = self.parent.register_change('delete_keyframe',
                                             {'frame':           frame,
                                              'animator_id':     animator_id,
                                              'attribute_name':  attribute_name,
                                              'attribute_value': attribute_value,
                                              'animator_type'  : animator_type})

    def on_layer_added(self, layer_id, queue_order):
        change = self.parent.register_change('add_layer',
                                             {'layer_id':   layer_id,
                                              'layer_name':  program_data['layers'][layer_id]['name'],
                                              'queue_order': queue_order})

    def on_layer_deleted(self, layer_id, layer_name):
        change = self.parent.register_change('delete_layer',
                                             {'layer_id':   layer_id,
                                              'layer_name':  layer_name,
                                              'queue_order': None})
        self.parent.add_side_effect('delete_effect', change, len(program_data['manager'].get_effects_in_layer(change['args']['layer_id'])), 'append')

    def on_layer_input_path_changed(self, layer_id, input_mode, input_path, old_input_mode, old_input_path):
        change = self.parent.register_change('change_layer_input_path',
                                             {'layer_id':   layer_id,
                                              'input_mode': input_mode,
                                              'input_path': input_path},
                                             {'layer_id':   layer_id,
                                              'input_mode': old_input_mode,
                                              'input_path': old_input_path})

    def on_keyframe_changed(self, frame, animator_id, attribute_name, attribute_value, old_attribute_value):
        change = self.parent.register_change('change_keyframe_value',
                                             {'frame':           frame,
                                              'animator_id':     animator_id,
                                              'attribute_name':  attribute_name,
                                              'attribute_value': attribute_value,
                                              'animator_type'  : program_data['keyframe_animator_types'][animator_id]},
                                             {'frame':           frame,
                                              'animator_id':     animator_id,
                                              'attribute_name':  attribute_name,
                                              'attribute_value': old_attribute_value,
                                              'animator_type'  : program_data['keyframe_animator_types'][animator_id]})

    def on_keyframe_moving(self, frame, new_frame, animator_id, attribute_name, delete):
        change_type = 'side_effect_keyframe_position'
        change = self.parent.register_change(change_type)
        if (new_frame in program_data['keyframes'] and
            animator_id in program_data['keyframes'][new_frame] and
            attribute_name in program_data['keyframes'][new_frame][animator_id]):

            self.parent.add_side_effect('change_keyframe', change, 1, 'append')
        else:
            self.parent.add_side_effect('add_keyframe', change, 1, 'append')
        if delete:
            self.parent.add_side_effect('delete_keyframe', change, 1, 'append')

    def on_keyframes_moving(self, count):
        change = self.parent.register_change('side_effect_keyframes_position')
        self.parent.add_side_effect('side_effect_keyframe_position', change, count, 'append')

    def on_keyframes_deleting(self, count):
        change = self.parent.register_change('side_effect_keyframes_deleted')
        self.parent.add_side_effect('delete_keyframe', change, count, 'append')
        self.parent.add_side_effect('change_keyframe_value', change, 'link:delete_keyframe', 'append')

    def on_keyframes_adding(self, count):
        change = self.parent.register_change('side_effect_keyframes_added')
        self.parent.add_side_effect('add_keyframe', change, count, 'append')
        self.parent.add_side_effect('change_keyframe_value', change, 'link:add_keyframe', 'append')

    def on_effect_name_changed(self, effect_id, effect_name, old_name, attribute_count):
        change = self.parent.register_change('change_effect_name',
                                             {'effect_id':   effect_id,
                                              'effect_name': effect_name},
                                             {'effect_id':   effect_id,
                                              'effect_name': old_name})
        self.parent.add_side_effect('side_effect_keyframes_added', change, 1, 'ignore')
        self.parent.add_side_effect('side_effect_keyframes_deleted', change, 1, 'append')

    def on_keyframe_target_added(self, animator_id, attribute_name):
        change = self.parent.register_change('add_keyframe_target',
                                             {'animator_id':    animator_id,
                                              'attribute_name': attribute_name})

    def on_keyframe_target_deleted(self, animator_id, attribute_name):
        change = self.parent.register_change('delete_keyframe_target',
                                             {'animator_id':    animator_id,
                                              'attribute_name': attribute_name})

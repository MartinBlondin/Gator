from PyQt5.QtCore import pyqtSlot
from PIL import Image
from PIL.ImageQt import ImageQt

from collections import OrderedDict

from backend.event_handlers.preview_event_handler import PreviewEventHandler

from utils.verify import verify
from utils.rm import rm
from utils.dict_place_or_create import dict_place_or_create
from utils.get_path_as_string import get_path_as_string

from definitions import program_data, EFFECT_COMMANDS, GRAPHICS_DIR, PREVIEW_SCALE_FACTOR, GRAPHICS_LOADING_IMAGE


class PreviewManagerMixin:
    def __init__(self):
        dict_place_or_create(program_data['preview_images'], '1', 'root', {'image': GRAPHICS_LOADING_IMAGE, 'status': 'complete', 'render_state': {}, 'renderer': None})
        dict_place_or_create(program_data['preview_images'], '1', 'merged', {'image': GRAPHICS_LOADING_IMAGE, 'status': 'complete', 'render_state': {}})

        super(PreviewManagerMixin, self).__init__()

        program_data['signals'].shutdown.connect(self.stop_all_preview_renderers)
        program_data['signals'].clear.connect(self.on_clear)
        program_data['manager'].add_event_handler('preview', PreviewEventHandler, self)

        self.preview_size = (int(program_data['animation_size'][0] * PREVIEW_SCALE_FACTOR), int(program_data['animation_size'][1] * PREVIEW_SCALE_FACTOR))

        self.waiting_for = {}  # used by cloning layers to make sure clone is rendered first

    def get_preview_input_image(self, frame, layer_id, render_state):
        if program_data['layers'][layer_id]['input_mode'] == 'Clone':
            related_layer = program_data['layers'][layer_id]['input_path']
            related_layer_last_frame = program_data['manager'].get_last_keyframe_for_layer(related_layer)
            if int(frame) > related_layer_last_frame: related_layer_frame = str(related_layer_last_frame)
            else:                                     related_layer_frame = frame

            if (related_layer in program_data['preview_images'][related_layer_frame].keys() and
                program_data['preview_images'][related_layer_frame][related_layer]['status'] == 'complete'):
                input_image = program_data['preview_images'][related_layer_frame][related_layer]['image']
            else:
                if not related_layer_frame + related_layer in self.waiting_for.keys():
                    self.waiting_for[related_layer_frame + related_layer] = []
                self.waiting_for[related_layer_frame + related_layer].append([frame, layer_id, render_state])
                return None

        elif program_data['layers'][layer_id]['input_mode'] == 'Video':
            input_image = Image.open(program_data['layers'][layer_id]['input_path'])

        else:
            input_image = Image.open(program_data['layers'][layer_id]['input_path'])

        return input_image

    def stop_all_preview_renderers(self):
        for __, layers in program_data['preview_images'].items():
            for layer_id, layer in layers.items():
                if layer_id != 'merged' and layer['renderer']:
                    layer['renderer'].stop()

    def stop_preview_renderer(self, frame, layer_id):
        if (frame in    program_data['preview_images'] and
            layer_id in program_data['preview_images'][frame] and
                        program_data['preview_images'][frame][layer_id]['renderer']):
            program_data['preview_images'][frame][layer_id]['renderer'].stop()

    def start_preview_render(self, frame, layer_id, render_state):
        if not render_state: return
        if not self.is_layer_renderable(layer_id): return

        self.stop_preview_renderer(frame, layer_id)

        dict_place_or_create(program_data['preview_images'], frame, layer_id,
                             {'image': GRAPHICS_LOADING_IMAGE, 'status': 'incomplete', 'renderer': None})
        dict_place_or_create(program_data['preview_images'], frame, 'merged',
                             {'image': GRAPHICS_LOADING_IMAGE, 'status': 'incomplete'})


        input_image = self.get_preview_input_image(frame, layer_id, render_state)
        if not input_image: return

        program_data['preview_images'][frame][layer_id]['renderer'] = program_data['manager'].render_layer(frame, layer_id,
                                                                                                           self.preview_size,
                                                                                                           render_state,
                                                                                                           input_image,
                                                                                                           self.complete_preview_render, [frame, layer_id])

        program_data['signals'].preview_changed.emit(frame, layer_id)
        program_data['signals'].preview_changed.emit(frame, 'merged')

    def complete_preview_render(self, rendered_image, frame, layer_id):
        program_data['preview_images'][frame][layer_id]['image'] = rendered_image
        program_data['preview_images'][frame][layer_id]['status'] = 'complete'
        program_data['signals'].preview_changed.emit(frame, layer_id)

        if frame + layer_id in self.waiting_for.keys():
            for args in self.waiting_for[frame + layer_id]:
                self.start_preview_render(*args)

        self.try_merge_layers(frame)

    def get_preview_layer_image_at_frame(self, frame, layer_id):
        last_keyframe_for_layer = program_data['manager'].get_last_keyframe_for_layer(layer_id)
        if int(frame) > last_keyframe_for_layer: frame = str(last_keyframe_for_layer)
        return program_data['preview_images'][frame][layer_id]['image']

    def try_merge_layers(self, frame):
        all_layers_complete = True
        for layer_id, layer in program_data['layers'].items():
            last_keyframe_for_layer = program_data['manager'].get_last_keyframe_for_layer(layer_id)
            if int(frame) > last_keyframe_for_layer: current_frame = str(last_keyframe_for_layer)
            else:                                    current_frame = frame

            if (not layer['input_path'] or
                layer_id in program_data['preview_images'][current_frame] and
                program_data['preview_images'][current_frame][layer_id]['status'] == 'incomplete' and layer_id != 'merged' or
                layer_id not in program_data['preview_images'][current_frame]):
                all_layers_complete = False

        if all_layers_complete:
            self.start_merge_layers(frame)
        else:
            program_data['preview_images'][frame]['merged']['image'] = GRAPHICS_LOADING_IMAGE
            program_data['preview_images'][frame]['merged']['status'] = 'incomplete'
            program_data['signals'].preview_changed.emit(frame, 'merged')

    def start_merge_layers(self, frame):
        image = self.get_preview_layer_image_at_frame(frame, 'root')
        layer_images = OrderedDict()
        for layer_id in filter(lambda l: l != 'root', program_data['layers'].keys()):
            layer_images[layer_id] = self.get_preview_layer_image_at_frame(frame, layer_id)

        program_data['manager'].merge_layers(frame, image, layer_images, self.complete_merge_layers)

    def complete_merge_layers(self, frame, image):
        program_data['preview_images'][frame]['merged']['image'] = image
        program_data['preview_images'][frame]['merged']['status'] = 'complete'
        program_data['signals'].preview_changed.emit(frame, 'merged')

    def on_clear(self):
        self.stop_all_preview_renderers()
        frames = list(program_data['preview_images'].keys())
        program_data['preview_images'] = {}
        for frame in frames:
            program_data['signals'].preview_changed.emit(frame, 'merged')
            program_data['signals'].preview_changed.emit(frame, program_data['selected_layer'])

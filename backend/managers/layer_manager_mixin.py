from collections import OrderedDict

from backend.managers.data_manager import manager_function_decorator

from utils.odict_insert_at_index import odict_insert_at_index
from utils.copy_dict import copy_dict
from utils.verify import verify

from definitions import program_data, LAYER_TEMPLATE, BLEND_MODES

class LayerManagerMixin:
    def __init__(self):
        program_data['layers'].update({'root': copy_dict(LAYER_TEMPLATE)})
        super(LayerManagerMixin, self).__init__()
        program_data['signals'].clear.connect(self.on_clear)

    @manager_function_decorator
    def add_layer(self, layer_name, queue_order=None, preset_id=False):
        if not queue_order: queue_order = len(program_data['layers'])
        if preset_id: layer_id = preset_id
        else:         layer_id = self.get_new_id()
        odict_insert_at_index(program_data['layers'], layer_id, copy_dict(LAYER_TEMPLATE), queue_order)
        program_data['layers'][layer_id]['name'] = layer_name
        program_data['signals'].layer_added.emit(layer_id, queue_order)

    @manager_function_decorator
    def delete_layer(self, layer_id):
        layer_name = program_data['layers'][layer_id]['name']
        program_data['layers'].pop(layer_id)

        for clone_layer_id, layer in program_data['layers'].items():
            if layer['input_path'] == layer_id:
                self.change_layer_input_path(clone_layer_id, None, None)

        if layer_id == program_data['selected_layer']:
            program_data['manager'].select_layer('root')

        program_data['signals'].layer_deleted.emit(layer_id, layer_name)

    @manager_function_decorator
    def select_layer(self, layer_id, emit=True):
        previous_layer = program_data['selected_layer']

        program_data['selected_layer'] = layer_id
        program_data['selected_layer_full'] = program_data['layers'][layer_id]
        if emit:
            program_data['signals'].layer_selected.emit(layer_id, previous_layer)

    @manager_function_decorator
    def rename_layer(self, layer_id, new_layer_name):
        program_data['layers'][layer_id]['name'] = new_layer_name
        program_data['signals'].layer_renamed.emit(layer_id, new_layer_name)

    @manager_function_decorator
    def change_layer_opacity(self, layer_id, opacity):
        program_data['layers'][layer_id]['opacity'] = opacity
        program_data['signals'].layer_opacity_changed.emit(layer_id, opacity)

    @manager_function_decorator
    def change_layer_blend_mode(self, layer_id, blend_mode):
        program_data['layers'][layer_id]['blend_mode'] = blend_mode
        program_data['signals'].layer_blend_mode_changed.emit(layer_id, blend_mode)

    @manager_function_decorator
    def change_layer_input_path(self, layer_id, input_mode, input_path):
        if not input_mode: input_mode = None
        if not input_path: input_path = None
        old_input_mode = program_data['layers'][layer_id]['input_mode']
        old_input_path = program_data['layers'][layer_id]['input_path']
        program_data['layers'][layer_id]['input_mode'] = input_mode
        program_data['layers'][layer_id]['input_path'] = input_path
        program_data['signals'].layer_input_path_changed.emit(layer_id, input_mode, input_path, old_input_mode, old_input_path)

    def change_layer_queue_order(self, layer_id, queue_order):
        layer = program_data['layers'].pop(layer_id)
        odict_insert_at_index(program_data['layers'], layer_id, layer, queue_order)
        program_data['manager'].emit(program_data['signals'].layer_queue_order_changed, [layer_id, queue_order])

    def is_layer_renderable(self, layer_id):
        if layer_id not in program_data['layers'].keys():
            return False
        if not program_data['layers'][layer_id]['input_path']:
            return False
        for effect_id in program_data['manager'].get_effects_in_layer(layer_id):
            if '1' in program_data['keyframes'].keys() and not effect_id in program_data['keyframes']['1']:
                return False
        if program_data['layers'][layer_id]['input_mode'] == 'Clone':
            related_layer = program_data['layers'][layer_id]['input_path']
            if not self.is_layer_renderable(related_layer):
                return False
        return True

    def on_clear(self):
        layers = copy_dict(program_data['layers'])
        program_data['layers'] = OrderedDict()
        program_data['layers'].update({'root': copy_dict(LAYER_TEMPLATE)})
        program_data['signals'].layer_input_path_changed.emit('root',
                                                              program_data['layers']['root']['input_mode'],
                                                              program_data['layers']['root']['input_path'],
                                                              None, None)
        program_data['manager'].select_layer('root')
        for layer_id, layer in layers.items():
            program_data['signals'].layer_deleted.emit(layer_id, layer['name'])

from threading import Thread
from collections import OrderedDict

from utils.copy_dict import copy_dict
from utils.verify import verify
from backend.renderer import Renderer

from definitions import program_data, EFFECT_COMMANDS, BLEND_MODES


class RenderManagerMixin:
    def __init__(self):
        super(RenderManagerMixin, self).__init__()

    def create_effect_render_command(self, frame, effect_id, command_format_dict):
        attributes = {}
        if effect_id in command_format_dict['state'].keys():
            for attribute_name, attribute in program_data['effects'][effect_id]['attributes'].items():
                attributes[attribute_name] = command_format_dict['state'][effect_id][attribute_name]
            command_format_dict['command'] = program_data['effects'][effect_id]['command'].format(**attributes)
        else:
            command_format_dict['command'] = ''

        if program_data['effects'][effect_id]['flag']:
            command_type = program_data['effects'][effect_id]['flag']
        else:
            command_type = 'base'

        return EFFECT_COMMANDS[program_data['effects'][effect_id]['name'].split('.')[0]][command_type].format(**command_format_dict)

    def render_layer(self, frame, layer_id, size, render_state, input_image, callback, callback_args=[], callback_kwargs={}):
        effects = program_data['manager'].get_effects_in_layer(layer_id)
        command_format_dict = {
            'width':  size[0],
            'height': size[1],
            'state':  render_state[0],
            'input_path':  '-.png',
            'output_path': '-.png',
        }

        commands = []
        for effect_id, effect in effects.items():
            commands.append(self.create_effect_render_command(frame, effect_id, command_format_dict).split(' '))

        if commands:
            return Renderer(commands, input_image, size, callback, callback_args, callback_kwargs)

        else:
            callback(input_image.resize(size).convert('RGBA'), *callback_args, **callback_kwargs)

    def merge_layers(self, frame, root_image, layer_images, callback, callback_args=[], callback_kwargs={}):
        def run_in_thread(frame, root_image, layer_images, callback, callback_args, callback_kwargs):
            merged_image = root_image
            for layer_id, layer_image in layer_images.items():
                opacity = program_data['layers'][layer_id]['opacity'] / 100
                blend_mode = program_data['layers'][layer_id]['blend_mode']
                merged_image = BLEND_MODES[blend_mode](merged_image, layer_image, opacity)
            callback(frame, merged_image, *callback_args, **callback_kwargs)
        Thread(target=run_in_thread, args=(frame, root_image, layer_images, callback, callback_args, callback_kwargs)).start()

    def make_video(self):
        pass

    def render_project(self):
        pass

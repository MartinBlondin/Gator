from definitions import program_data
from utils.verify import verify

def clean_position_value(value, attribute_type, size_matters):
    size = program_data['window_size']
    if size_matters:
        if attribute_type.split(':')[2] == 'real':
            if attribute_type.split(':')[1] == 'x':
                value = size[0] * value
            else:
                value = size[1] * value
        elif attribute_type.split(':')[2] == 'percentage':
            value = int(value * 100)
    else:
        value = value
    return value

def get_last_attribute_value_at_frame(target_frame, animator_id, attribute_name, keyframes):
    frame = 1
    target_frame = int(target_frame)
    last_key = -1

    for key, keyframe in keyframes.items():
        key = int(key)
        if key != 1 and key < target_frame and key > last_key:
            if animator_id in keyframe.keys():
                if attribute_name in keyframe[animator_id].keys():
                    frame = key
                    last_key = key

    value = keyframes[str(frame)][animator_id][attribute_name]

    return frame, value

def get_next_attribute_value_at_frame(target_frame, animator_id, attribute_name, keyframes):
    frame = 1
    target_frame = int(target_frame)
    last_key = max(map(int, keyframes.keys()))

    for key, keyframe in keyframes.items():
        key = int(key)
        if key != 1 and key > target_frame and key <= last_key:
            if animator_id in keyframe.keys():
                if attribute_name in keyframe[animator_id].keys():
                    frame = key
                    last_key = key

    value = keyframes[str(frame)][animator_id][attribute_name]

    return frame, value

def calculate_attribute_value_at_frame(frame, animator_id, attribute_name, attribute_type, size_matters, keyframes):
    last_frame, last_value = get_last_attribute_value_at_frame(frame, animator_id, attribute_name, keyframes)
    next_frame, next_value = get_next_attribute_value_at_frame(frame, animator_id, attribute_name, keyframes)
    last_value, next_value = float(last_value), float(next_value)
    frame = int(frame)

    value = last_value

    if attribute_type in ['int', 'float'] or attribute_type.split(':')[0] == 'position':
        if frame > next_frame:
            pass
        elif last_frame != next_frame:
            frame_distance = frame - last_frame
            value_per_frame = 0

            if last_value < next_value:
                value_per_frame = (next_value - last_value) / (next_frame - last_frame)
            elif last_value > next_value:
                value_per_frame = (last_value - next_value) / (next_frame - last_frame)
                value_per_frame *= -1

            value = last_value + (value_per_frame * frame_distance)  # linear

        if attribute_type == 'int':
            value = int(value)
        elif attribute_type.split(':')[0] == 'position':
            value = clean_position_value(value, attribute_type, size_matters)

    elif attribute_type == 'combo':
        value = int(value)

    elif attribute_type == 'invisible':
        value = program_data['effects'][animator_id]['attributes'][attribute_name]['value']

    return value


class AttributeValueManagerMixin:
    def __init__(self):
        super(AttributeValueManagerMixin, self).__init__()

    def get_attribute_value_at_frame(self, frame, animator_id, attribute_name, size_matters=False):
        keyframes = program_data['keyframes']
        attribute_type = program_data[f'{program_data["keyframe_animator_types"][animator_id]}s'][animator_id]['attributes'][attribute_name]['type']
        if animator_id not in program_data['keyframe_targets'] or attribute_name not in program_data['keyframe_targets'][animator_id]:
            frame = '1'

        if (frame in keyframes.keys() and
            animator_id in keyframes[frame].keys() and
            attribute_name in keyframes[frame][animator_id].keys()):

            value = keyframes[frame][animator_id][attribute_name]
            if attribute_type.split(':')[0] == 'position':
                value = clean_position_value(value, attribute_type, size_matters)
        else:
            value = calculate_attribute_value_at_frame(frame, animator_id, attribute_name, attribute_type, size_matters, keyframes)

        for generator_id in program_data['manager'].get_generators_by_connection(animator_id, attribute_name):
            connection_name = program_data['generators'][generator_id]['connections'][animator_id][attribute_name]['Amplitude']
            value *= (program_data['manager'].get_generator_value(frame, generator_id) *
                      program_data['manager'].get_attribute_value_at_frame(frame, generator_id, connection_name) + 1)

        return value

from PyQt5.QtGui import QImage

from collections import OrderedDict

from utils.verify import verify
from utils.write_dict_to_json import write_dict_to_json
from utils.copy_dict import copy_dict

import demjson

from definitions import program_data, EFFECT_COMMANDS


class StateManagerMixin:
    def __init__(self):
        super(StateManagerMixin, self).__init__()

    def save(self, path=None):
        if not path: path = program_data['last_save_file_name']
        if path.split('.')[-1] != 'gator':
            path += '.gator'
        program_data['last_save_file_name'] = path
        program_data['last_save_change'] = program_data['current_change']
        data = copy_dict(program_data)
        data.pop('preview_images'); data.pop('manager'); data.pop('signals');
        data.pop('window'); data.pop('window_size'); data.pop('logger')
        data.pop('render_states'); data.pop('event_handlers'); data.pop('stats')
        write_dict_to_json(data, path)

    def load(self, path=None):
        if not path: path = program_data['last_save_file_name']
        with open(path, 'r') as f:
            new_data = demjson.decode(f.read())[0]

        program_data['manager'].clear()
        program_data['manager'].select_keyframe(new_data['selected_frame'])
        program_data['manager'].set_fps(new_data['fps'])
        program_data['manager'].set_animation_size(new_data['animation_size'][0], new_data['animation_size'][1])

        program_data['manager'].change_layer_blend_mode('root', new_data['layers']['root']['blend_mode'])
        program_data['manager'].change_layer_opacity('root', new_data['layers']['root']['opacity'])
        program_data['manager'].change_layer_input_path('root', new_data['layers']['root']['input_mode'],
                                                        new_data['layers']['root']['input_path'])
        for layer_id, layer in new_data['layers'].items():
            if layer_id == 'root': continue
            program_data['manager'].add_layer(layer_id, preset_id=layer_id)
            program_data['manager'].change_layer_blend_mode(layer_id, layer['blend_mode'])
            program_data['manager'].change_layer_opacity(layer_id, layer['opacity'])
            program_data['manager'].change_layer_input_path(layer_id, layer['input_mode'], layer['input_path'])

        program_data['manager'].select_layer(new_data['selected_layer'])

        for effect_id, effect in new_data['effects'].items():
            program_data['manager'].add_effect(effect['name'], effect['layer'], preset_id=effect_id)

        for frame, animators in new_data['keyframes'].items():
            for animator_id, attributes in animators.items():
                for attribute_name, attribute_value in attributes.items():
                    program_data['manager'].add_keyframe(frame, animator_id, attribute_name, attribute_value)

        for animator_id, attributes in new_data['keyframe_targets'].items():
            for attribute_name in attributes:
                program_data['manager'].add_keyframe_target(animator_id, attribute_name)

        program_data['last_save_file_name'] = path

import matplotlib.pyplot as plt

import logging

from definitions import program_data


class LoggingManagerMixin:
    def __init__(self, *args, **kwargs):
        program_data['logger'] = logging.getLogger(__name__)
        # program_data['logger'].setLevel(logging.INFO)

        super(LoggingManagerMixin, self).__init__(*args, **kwargs)

    def logger_register_function_call(self, fun, args, kwargs):
        fun = str(fun).split(' ')[1]
        if kwargs:
            program_data['logger'].info('\nCalled {} {}\n {}'.format(fun, args, kwargs))
        else:
            program_data['logger'].info('\rCalled {} {}'.format(fun, args))

    def plot(self, data):
        fig, ax = plt.subplots()
        ax.barh(list(data.keys()), [item for __, item in data.items()])

    def stats(self):
        process_time_data = {}
        for function_call in program_data['stats']['function_calls']:
            name = function_call['function_name']
            if name in process_time_data: process_time_data[name] = (function_call['process_time'] + process_time_data[name]) / 2
            else:                         process_time_data[name] = function_call['process_time']
        self.plot(process_time_data)
        plt.show()

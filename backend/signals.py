from PyQt5.QtCore import QObject, pyqtSignal


class Signals(QObject):
    '''
    Exposed to the rest of the program through program_data['signals']
    see data_manager.py
    '''
    # global
    shutdown = pyqtSignal()
    clear    = pyqtSignal()

    # window
    window_changed      = pyqtSignal(object)    # window
    window_size_changed = pyqtSignal(int, int)  # width, height

    # render
    animation_size_changed = pyqtSignal(int, int)  # width, height
    fps_changed            = pyqtSignal(int)       # fps

    # layers
    layer_added               = pyqtSignal(str, int) # layer_id, queue_order
    layer_deleted             = pyqtSignal(str, str) # layer_id, layer_name
    layer_selected            = pyqtSignal(str, str) # layer_id, previous_layer_id
    layer_renamed             = pyqtSignal(str, str) # layer_id, new_layer_name
    layer_blend_mode_changed  = pyqtSignal(str, str) # layer_id, blend_mode
    layer_opacity_changed     = pyqtSignal(str, int) # layer_id, opacity
    layer_queue_order_changed = pyqtSignal(str, int) # layer_id, queue_order
    layer_input_path_changed  = pyqtSignal(str, object, object, str, str)  # layer_id, input_mode, input_path, old_input_mode, old_input_path

    # effects
    effect_added        = pyqtSignal(str, int)            # effect_id, queue_order
    effect_deleted      = pyqtSignal(str, str, str)       # effect_id, effect_Name, layer_id
    effect_name_changed = pyqtSignal(str, str, str, int)  # effect_id, effect_name, old_name, attribute_count

    # keyframes
    keyframe_added    = pyqtSignal(str, str, str, object, str)     # frame, animator_id, attribute_name, attribute_value, animator_type
    keyframe_changed  = pyqtSignal(str, str, str, object, object)  # frame, animator_id, attribute_name, attribute_value, old_attribute_value
    keyframe_deleted  = pyqtSignal(str, str, str, object, object)  # frame, animator_id, attribute_name, attribute_value, animator_type
    keyframe_selected = pyqtSignal(str)                            # frame
    keyframe_moving   = pyqtSignal(str, str, str, str, bool)       # frame, new_frame, animator_id, attribute_name, delete_old,

    keyframes_moving   = pyqtSignal(int)  # count
    keyframes_deleting = pyqtSignal(int)  # count
    keyframes_adding   = pyqtSignal(int)  # count

    keyframe_target_added   = pyqtSignal(str, str)     # animator_id, attribute_name
    keyframe_target_deleted = pyqtSignal(str, object)  # animator_id, attribute_name(can be None)

    # generators
    generator_added              = pyqtSignal(str, int)      # generator_id, queue_order
    generator_deleted            = pyqtSignal(str)           # generator_id
    generator_connection_added   = pyqtSignal(str, str, str) # generator_id, animator_id, attribute_name
    generator_connection_deleted = pyqtSignal(str, str, str) # generator_id, animator_id, attribute_name
    generator_type_changed       = pyqtSignal(str, str, str) # generator_id, type, old_type

    # render_states
    render_state_changed = pyqtSignal(str, str, object)  # frame, layer_id, render_state

    # preview
    preview_changed = pyqtSignal(str, object)  # frame, layer_id(can also be 'merged' or None)

    # playback
    playback_frame_changed   = pyqtSignal(str)   # frame
    playback_playing_changed = pyqtSignal(bool)  # playing

    # history
    history_changed = pyqtSignal(int, str, object)  # change_id, change['type'], change['args']

from PIL import Image
from io import BytesIO
from subprocess import run, PIPE

from utils.queue import queue

from definitions import MAX_RENDER_THREADS


class Renderer:
    def __init__(self, commands, input_image, size, callback, callback_args, callback_kwargs):
        self.stopped = False
        self.start(commands, input_image, size, callback, callback_args, callback_kwargs)

    @queue(MAX_RENDER_THREADS)
    def start(self, commands, input_image, size, callback, callback_args, callback_kwargs):
        if self.stopped: return

        image = input_image
        for command in commands:
            image_bytes = BytesIO(); image.save(image_bytes, format='png')
            process = run(command, input=image_bytes.getvalue(), stdout=PIPE, stderr=PIPE)
            image = Image.open(BytesIO(process.stdout)).convert('RGBA')
            if self.stopped: return
        callback(image, *callback_args, **callback_kwargs)

    def stop(self):
        self.stopped = True
